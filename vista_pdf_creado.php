<?php 
    include "consultas/vista_pdf_creado.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>F360</title>
</head>
<body>
    <?php include "header.php" ?>
    
    <div class="container">
        <input type="hidden" id="id_solicitud" value="<?php echo $_GET['folio_servicio'] ?>">                  
        <div id="pages-container">     

            <div class="row print-wrap page1">          
                <div class="col-sm-6">
                    <section class="portada-izq">                    
                    </section>
                </div>     
                <div class="col-sm-6">
                    <section class="portada-der"></section>
                    <br>
                    <h1 style="font-size: 3.0rem"><b>Reporte Feddback 360°</b></h1>
                    <h3 style="font-size: 3.0rem"><?php echo $fecha_a_mostrar ?></h3>
                    <section class="portada-der-linea-white"></section>
                    <section class="portada-der-linea"></section>
                    <br>
                    <h2 style="font-size: 3.0rem"><b>Informe a</b></h2>
                    <h3 style="font-size: 2.5rem"> <?php echo $participante ?> </h3>
                    <br><br>
                    <section class="portada-der-bottom">
                        <img src="img/logo_portada.png" alt="F360" class="imagen_portada">
                    </section>
                </div>                                                
            </div>
        
            <!-- pagina 2 indice -->
            <div class="row print-wrap page2">
                <div class="col-12 text-right">            
                    <img src="img/feedback_360.jpg" alt="feedback 360" height="70px">
                </div>
                <div class="col-12 text-center">            
                    <img src="img/feedback_360.jpg" alt="feedback 360" height="150px">
                </div>             
                <div class="col-12">
                    <section>
                        <h1 style="color:#526a9b"><b>Contenido</b></h1>
                    </section>
                    <br><br>
                    <ul>               
                        <li><h3><b>PERFIL DEL PARTICIPANTE</b><b style="float: right">3</b></h3></li>
                        <li><h3><b>PRESENTACIÓN FEEDBACK 360°</b><b style="float: right">4</b></h3></li>                          
                        <span id="li-naturalezas"></span>
                        <li><h3><b>PERCEPCIÓN DEL INDIVIDUO</b><b style="float: right">12</b></h3></li>
                        <li><h3><b>EVALUACIÓN 360°</b><b style="float: right">16</b></h3></li>
                        <li><h3><b>PLAN DE MEJORA</b><b style="float: right">18</b></h3></li>
                    </ul>                    
                </div>
                <div class="col-12" style="color: white"></div>
                <div class="col-12" style="color: white"></div>
                     
            </div >

            <!-- pagina 3 organigrama -->
            <div class="row print-wrap page3">
                <div class="col-12 text-right">            
                    <img src="img/feedback_360.jpg" alt="feedback 360" height="70px">
                </div>
                <div class="col-12">
                    <h1 style="color:#526a9b">PERFIL DEL PARTICIPANTE</h1>
                    <br>
                </div>

                <div class="col-12">
                    <section style="border: 1px #d1cdcd solid; border-radius: 5px; padding-top: 20px; padding-bottom: 20px; margin-bottom: 25px">
                        <ul>
                            <li><b style="font-size: 2rem">Nombre: </b><span style="font-size: 1.5rem" id="nombre_perfil_participante"></span></li>
                            <li><b style="font-size: 2rem">Puesto: </b><span style="font-size: 1.5rem" id="puesto_perfil_participante"></span></li>
                            <li><b style="font-size: 2rem">Correo: </b><span style="font-size: 1.5rem" id="correo_perfil_participante"></span></li>                            
                        </ul>
                    </section>
                </div>                
                <div class="col-12 text-center">
                    <h3><b>Lugar en el organigrama</b></h3>
                    <div id="can_img_organigrama"></div>
                </div>
                <div class="col-12" style="color: white"></div>
                <div class="col-12" style="color: white"></div>
               
            </div>

            <!-- Pagina 4 presentacion feedback -->
            <div class="row print-wrap page4">
                <div class="col-12 text-right">            
                    <img src="img/feedback_360.jpg" alt="feedback 360" height="70px">
                </div>
                <div class="col-12">
                    <h1 style="color:#526a9b">PRESENTACIÓN FEEDBACK 360°</h1>
                    <br>
                </div>                            
                <div class="col-12" style="font-size: 1.2rem">
                    <p>La evaluación 360 se realizó por medio de dos pares y tres colaboradores de Miguel, Soto la cual se compara con la autoevaluación realizada por el participante para poder generar un plan de mejora. </p>
                    <p>Con el fin de mantener la confiabilidad de la prueba y darle una mejor la retroalimentación al participante sólo se muestran los resultados generales de la prueba.</p>
                    <p>Las 13 competencias con las que fue evaluado están divididas en tres grandes grupos de acuerdo a su naturaleza, las 13 competencias aparecen en seguida y se definen a lo largo del documento para dar mayor claridad a los aspectos en lo que fue evaluado.</p>
                </div>
                <div class="col-12" style="font-size: 1.2rem">
                    <table class="table table-bordered" id="tbl_competencias">
                        <thead><tr></tr></thead>
                        <tbody></tbody>
                    </table>
                </div>
                <div class="col-12" style="font-size: 1.2rem">
                    <p>Mariano Vilallonga expone 10 razones principales para utilizar las evaluaciones 360° en las compañías.</p>
                    <p>1.Realizar un diagnóstico correcto</p>
                    <p>2.Construir planes de mejora bien enfocados</p>
                    <p>3.Información complementaria a la evaluación del desempeño</p>
                    <p>4.Hallar el potencial de mejora de una persona</p>
                    <p>5.Planes de promoción interna</p>
                    <p>6.Auditar la calidad directiva de la empresa</p>
                    <p>7.Realizar plan de formación y desarrollo de plantilla</p>
                    <p>8.Adecuar perfiles directivos a la estrategia</p>
                    <p>9.En procesos de fusión o absorción</p>
                    <p>10.Transmitir serenidad y enfoque</p>
                    <br>
                    <p>El artículo completo aparece en nuestro blog: <a href="http://bit.ly/145iYXP">http://bit.ly/145iYXP</a></p>
                </div>
                <!-- pagina 5 escala de calificacion -->
                <div class="col-12"> 
                    <h1 style="color:#526a9b">Escala de Calificación</h1>
                </div>
                <div class="col-12">
                    <table class="table table-bordered" style="font-size: 1.1rem">
                        <thead>
                            <tr>
                                <th>1</th>
                                <th>2</th>
                                <th>3</th>
                                <th>4</th>
                                <th>5</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Usted tiene una competencia que no ha desarrollado.</td>
                                <td>Usted ha desarrollado poco esa competencia.</td>
                                <td>El nivel de desarrollo de competencia es un nivel promedio.</td>
                                <td>Usted ha desarrollado muy buena forma esa competencia.</td>
                                <td>Usted tiene perfectamente desarrollada esa competencia.</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            
        </div>
    </div>
    <?php include "footer.php"?>
    <script src="js/vista_pdf_creado.js"></script>
    <script src="js/crear_pdfs.js"></script>
</body>
</html>