<!DOCTYPE html>
<html lang="en">
<?php include "header.php" ?>
<body>
    <?php include "nav.php" ?>  
    <div class="d-flex" id="wrapper">
        <?php include "sidebar.php"?>   
        <div id="page-content-wrapper">   
            <?php include "modales.php"?>    
            <div class="container-fluid">  
                <div class="col-12 d-flex flex-wrap flex-md-nowrap align-items-center pt-3 mb-3 border-bottom">
                    <div class="col-sm-8">
                        <button class="btn" id="btn-sidebar" title="Campos disponibles"><i class="fas fa-bars"></i></button>
                        <h1 class="h2">Mi Perfil</h1>
                    </div>
                    <div class="col-sm-4 text-right">
                        <button class="btn btn-sm btn-outline-secondary">Editar</button>
                    </div>
                </div>                   
                <div class="row">
                    <div class="col-sm-12 col-md-3 col-lg-3">   
                        <section style="border: 10px solid #d7d7d7; border-radius: 10px">
                            <img src="<?php echo $imgUsuario?>" alt="foto" style="max-width: 100%"><br>
                        </section>                       
                    </div>
                    <div class="col-sm-12 col-md-9 col-lg-9">                        
                            <span style="font-size: 2rem; color: #16195c "><b><?php echo $nombres." ".$apellidos ?></b></span><br>                                                                                                                                                   
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include "footer.php" ?>
    <script src="js/perfil_usuario.js"></script>
</body>
</html>