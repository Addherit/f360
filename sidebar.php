<?php 
  session_start();
  $nombres = $_SESSION["nombreUsuario"];
  $apellidos = $_SESSION["apellidosUsuario"];

  $imgUsuario = "img/usuarios/usuario.jpg";
  $existe = (is_file($imgUsuario));

  if($existe){ 
    $imgUsuarioCompuesta = '<img src="'.$imgUsuario.'" title="Ver perfil" style="max-width: 10rem; border-radius: 10rem; border: 1px solid #b0b0b0;">';
  } else {
    $imgUsuarioCompuesta = '<i class="fas fa-user-circle" style="font-size:10rem; color: black" title="ver perfil"></i>'; 
  }  
?>
<div class="border-right" id="sidebar-wrapper">
  <br>    
  <div class="list-group list-group-flush">
    <div class="row">
      <div class="col-md-12 text-center">
        <a href="perfil_usuario.php" style="text-decoration: none; color: #585757">                     
          <?php echo $imgUsuarioCompuesta ?>            
          <br>          
          <span style="color: white"><?php echo $nombres." ".$apellidos?></span>
          <br><br><br>
        </a>
        <input type="text" value="<?php echo $codEmpleado ?>" id="codEmpleado" hidden>        
      </div>
    </div>
    <div class="row">
        <div class="col-12">
          <ul class="nav sidebar-options">
            <li class="nav-item">
              <a class="nav-link inactive-sidebar" href="solicitud_de_servicios.php">
                <i class="fas fa-bell"></i>
                <span>Notificaciones de servicios</span> 
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link inactive-sidebar" href="catalogo_de_encuestas.php">
              <i class="fas fa-list"></i>
                <span>Catálogo de encuestas</span> 
              </a>
            </li>

            <li class="nav-item">
              <a class="nav-link inactive-sidebar" href="competencias.php">
              <i class="fas fa-chess-bishop"></i>
                <span>Competencias</span> 
              </a>
            </li>      

            <li class="nav-item">
              <a class="nav-link inactive-sidebar" href="crear_reportes.php">
              <i class="fas fa-chart-pie"></i>
                <span>Crear reportes</span> 
              </a>
            </li>                
            
          </ul> 
        </div>    
    </div>
  </div>
</div>