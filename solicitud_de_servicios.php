<!DOCTYPE html>
<html lang="en">
<?php include "header.php" ?>
<body onload="consultar_solicitud_de_servicios()">
    <?php include "nav.php" ?>
    <div class="d-flex" id="wrapper">
        <?php include "sidebar.php"?>   
        <div id="page-content-wrapper">   
            <?php include "modales.php"?>    
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 d-flex flex-wrap flex-md-nowrap align-items-center pt-3 mb-3 border-bottom">

                        <div class="col-sm-8">
                            <button class="btn" id="btn-sidebar" title="Campos disponibles"><i class="fas fa-bars"></i></button>
                            <h2 class="titulo-vistas">Solicitud de servicios</h2>                        
                        </div>
                        <div class="col-sm-4 text-right">
                        <button class="btn btn-sm btn-info" data-toggle="modal" data-target="#modal_mandar_formulario">Mandar Formulario</button>
                        </div>
                        
                    </div>  
                </div>
                <div class="row">                                                 
                    <div class="col-md-12"> 
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fas fa-search"></i></span>
                            </div>
                            <input id="filtroEquipoDeTrabajo" type="text" name="valorEscrito" class="form-control col-md-6 filtroBusqueda" placeholder="Buscar por ID, Nombre, correo electrónico o estatus" onkeyup="consultar_solicitud_de_servicios()">
                            <div class="spin" style="margin-left: 5px; display: none"><span class="spinner"></span></div>                                                
                        </div>
                        <div class="mensaje"></div>
                        <div class=" table-responsive">
                            <table class="table table-striped table-sm table-bordered table-hover text-center" id="tbl_solicitud_servicios" style="white-space: nowrap">
                                <thead style="background-color: #16195c; color: white">
                                    <tr>
                                        <th colspan=2></th>                                    
                                        <th scope="col">ID</th>
                                        <th scope="col">Nombre del solicitante</th>
                                        <th scope="col">Correo electrónico</th>
                                        <th scope="col">Tipo de servicio</th>
                                        <th scope="col">Fecha de solicitud</th>
                                        <th scope="col">Estatus</th>        
                                        <th></th>                            
                                    </tr>
                                </thead>
                                <tbody>                                
                                </tbody>
                            </table>
                        </div>                    
                    </div>
                </div>            
            </div>
        </div>
    </div>
    <?php include "footer.php" ?>
    <script src="js/solicitud_de_servicios.js"></script>
</body>
</html>