<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="icon" type="img/" href="img/favicon.ico">
    <title>F360</title>
</head>
<body class="bg">
    <nav class="navbar navbar-light header-color">
        <a class="navbar-brand" href="#">
            <img src="img/f360.png" class="d-inline-block align-top" alt="F360"  style="width: 250px">
        </a>
    </nav>    
    <div class="container">
        <div class="row padding-top">
            <div class="col-md-4 col-lg-6 col-xl-7">
                <h1 class="text-center">Portal de gestión de servicios F360</h1>
                <p class="col-md-12" style="font-size:1.2rem;">Sistema creado en el año 2018, con la finalidad de llevar control con los servicios brindados al cliente por parte de F360.</p>
            </div>
            <div class="col-md-8 col-lg-6 col-xl-5">
                <section class="text-center sectionLogin">
                    <div class="container">
                        <div class="padding-top">
                            <i class="fas fa-user-circle" style="font-size:8rem"></i><br>
                            <span>Inicia Sesión</span><br>                                
                            <div class="row mensaje"></div>
                            <div class="col-sm-2 offset-5 spin" style="margin-top: 10px;"><span class="spinner"></span></div>
                        </div>
                        <div class="row padding-top padding-bottom">
                            <div class="col-md-8 offset-md-2 text-left"> 
                                <div class="input-group form-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-user"></i></span>
                                    </div>
                                    <input id="cuentaCorreo" type="text" class="form-control form-control-sm" placeholder="username">                                        
                                </div>                                                                      
                                <div class="input-group form-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-key"></i></span>
                                    </div>
                                    <input id="cuentaPassword" type="password" class="form-control form-control-sm" placeholder="password">
                                </div>                                    
                                <br><br>
                                <button type="submit" onclick="validarCorreoYpassword()" class="btn btn-info btn-lg btn-block">Iniciar Sesión</button>                                                                        
                            </div>
                        </div>                            
                    </div>
                </section>
                <span style="font-size: .8rem; color: grey">Versión 1.0 Ultima actualización 21 de agosto del 2019</span>
            </div>
        </div>        
    </div>    
    <?php include "footer.php" ?>
    <script type="text/javascript" src="js/login.js"></script>   
</body>
</html>

