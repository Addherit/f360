<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>F360</title>
</head>
<body onload="consultar_preguntas_respuestas()">
    <?php include "header.php" ?>
    <div class="container-fluid">
        <div class="row">
            <?php include "sidebar.php"?>   
            <?php include "modales.php"?>          
            <main role="main" class="col-md-9 col-lg-9 col-xl-10 ml-sm-auto">
                <input type="hidden" value="<?php echo $_GET['id_solicitud'] ?>" id="id_solicitud">
                <div class="row justify-content-between align-items-center pt-3 mb-3 border-bottom">
                    <div class="col-sm-8">
                        <h1 class="h2">Vista previa del servicio N° <span><?php echo $_GET['id_solicitud'] ?></span></h1>     
                    </div>        
                    <div class="col-sm-3 text-right">
                        <button class="btn btn-sm btn-info" data-toggle="modal" data-target="#modal_vista_previa_servicios">Crear comentarios finales</button>
                    </div>        
                </div> 
                <div class="row">
                    <div class="col-md-6" style="height:645px; overflow: auto">
                        <table class="table table-hover table striped table-sm" id="tbl-preguntas-respuestas-container">
                            <thead>
                                <tr>
                                    <th>N° Pregunta</th>
                                    <th>Pregunta</th>
                                    <th>Respuesta</th>
                                    <th>Nombre Participante</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>                                              
                    <div class="col-md-6">
                        <table class="table table-sm table-hover table-striped" id="table-autoevaluacion">
                            <thead class="text-center">
                                <tr>
                                    <th colspan="2" >Autoevaluación</th>
                                </tr>
                                <tr>
                                    <th>Fortalezas</th>
                                    <th>Áreas de oportunidad</th>
                                </tr>
                            </thead>
                            <tbody class="text-center">                        
                            </tbody>
                        </table>
                        <hr>
                        
                    </div>
                </div>
                
            </main>
        </div>
    </div>
    <?php include "footer.php" ?>
    <script src="js/vista_previa_respuestas.js"></script>
</body>
</html>