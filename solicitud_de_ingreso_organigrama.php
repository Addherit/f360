<!DOCTYPE html>
<html lang="en">
<?php include "header.php" ?>
  <body>
    <?php include "nav.php" ?>      
    <div class="container-fluid">
      <input type="hidden" id="nombre_usuario" value="<?php echo $_GET['name']?>">
      <input type="hidden" id="correo_electronico" value="<?php echo $_GET['email']?>">
      <input type="hidden" id="folio" value="<?php echo $_GET['folio']?>">

      <div class="row">
        <div class="col-lg-8">
          <div id="chart-container">
          </div>
        </div>
        <div class="col-lg-4">
          <div id="edit-panel">              
            <div class="form-group">
              <label for="selected-node">Colaborador Root</label>
              <input type="text" class="selected-node-group form-control form-control-sm" id="selected-node" value="<?php echo $_GET['name']?>" disabled>
              <small class="form-text text-muted">Este es el colaborador que será evaluado, no puede haber nodos <b>Iguales</b> si no existe un nodo <b>Jefe</b>.</small>
            </div>                        

            <div class="form-group">
              <label >Colaborador:</label>
              <div id="new-nodelist">
                <input type="text" class=" new-node form-control form-control-sm" placeholder="Nombre completo">
              </div>
            </div>

            <div class="form-group">
              <label for="staticEmail">E-mail:</label>
              <div id="new-nodelist-email">
                <input type="text" class="new-node-email form-control form-control-sm" placeholder="Correo electrónico">
              </div>
            </div>
            <div class="tetxt-center">
            <br>
              <span id="node-type-panel" class="radio-panel">
              <input type="radio" name="node-type" id="rd-parent" value="parent"><label for="rd-parent">Padre</label>
              <input type="radio" name="node-type" id="rd-child" value="children"><label for="rd-child">Hijo</label>
              <input type="radio" name="node-type" id="rd-sibling" value="siblings"><label for="rd-sibling">Hermano</label>
              </span>
            </div>
             

              

              <div class="col-md-12 text-right">
                <br>
                <button type="button" class="btn btn-sm btn-success" id="btn-add-nodes"><i class="fas fa-plus-circle"></i> Agregar</button>
                <button type="button" class="btn btn-sm btn-danger" id="btn-delete-reload"><i class="fas fa-trash"></i> Borrar</button>
                <!-- <button type="button" class="btn btn-sm btn-light" id="btn-reset"><i class="fas fa-sync"></i> Limpiar</button>                 -->
              </div>
            </div>                                  
          </div>          
          <br>               
        </div>         
      </div>          
    </div>


    <?php include "footer.php" ?>        
    <script src="js/solicitud_de_servicios_organigrama.js"></script>
  </body>
</html>