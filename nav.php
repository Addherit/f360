<nav class="navbar navbar-expand-lg navbar-dark sticky-top header header-color">
    <a class="navbar-brand" href="#"><img src="img/f360.png" class="d-inline-block align-top" alt="F360" style="width: 250px"></a>
    <div id="navbarSupportedContent" style="flex-grow: 1;  display: flex!important;">
        <span class="mr-auto"></span>
        <div class="form-inline my-2 my-lg-0">
            <a href="logOut.php" style="text-decoration: none" id="btn-logout">
                <i class="fas fa-sign-out-alt" style="color: white"></i>
                <span style="color: white">Salir</span>
            </a>
        </div>
    </div>
</nav>