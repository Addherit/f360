<!DOCTYPE html>
<html lang="en">
<?php include "header.php" ?>
<body onload="consultar_competencia()">
    <?php include "nav.php" ?>
    <div class="d-flex" id="wrapper">
        <?php include "sidebar.php"?>   
        <div id="page-content-wrapper">   
            <?php include "modales.php"?>    
            <div class="container-fluid">   
                <div class="row">
                    <div class="col-12 d-flex flex-wrap flex-md-nowrap align-items-center pt-3 mb-3 border-bottom">                        
                        <button class="btn" id="btn-sidebar" title="Campos disponibles"><i class="fas fa-bars"></i></button>
                        <h1 class="h2 col-md-6">Competencias</h1>                                          
                    </div>  
                </div>
                <div class="col-md-12"> 
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-search"></i></span>
                        </div>
                        <input id="filtro_competencias" type="text" class="form-control col-md-6 filtroBusqueda" placeholder="Buscar competencia" onkeyup="consultar_competencia()">
                        <div class="spin" style="margin-left: 5px; display: none"><span class="spinner"></span></div><br>
                        
                        
                    </div>                   
                    <div class="mensaje"></div>                            
                    <div class=" table-responsive">
                        <table class="table table-striped table-sm table-bordered table-hover text-center" id="tbl_competencias" style="white-space: nowrap">
                            <thead style="background-color: #16195c; color: white">
                                <tr>
                                    <th colspan=2></th>                                    
                                    <th>ID</th>                                               
                                    <th>ID de encuesta</th>             
                                    <th>Nombre de competencia</th>
                                    <th>Estatus</th>
                                </tr>
                            </thead>
                            <tbody>                                
                            </tbody>
                        </table>
                    </div>                    
                </div>                                
            </div>
        </div>
    </div>
    <?php include "footer.php" ?>
    <script src="js/competencias.js"></script>
</body>
</html>