<?php 
    if(isset($_GET['id'])){
        $folio = $_GET['id'];
    } else {
        $folio = FALSE;
    }

?>
<!DOCTYPE html>
<html lang="en">
<?php include "header.php" ?>    
<body onload="validar_folio_inicial()">
<?php include "nav.php" ?>    
    <div class="container">
        <div class="row">                                    
            <div class="col-md-12 pt-3 mb-3">
                <h1 class="h2">Solicitud de servicios</h1>  
                <hr>                   
            </div>                                   
            <div class="col-md-12"> 
                <div class="input-group mb-3">                                        
                    <div class="spin" style="margin-left: 5px; display: none"><span class="spinner"></span></div>
                    <div class="row mensaje"></div>                    
                </div>
            </div>            
        </div>
        <div class="row">
            <div class="col-md-12">                            
                <form method="post" id="formulario_queen" onsubmit="guardarDatos()">
                    
                    <div class="form-control" style="background-color: #ced4da; margin-bottom: 10px">
                        <span><b>l. Del Interesado</b></span>
                        <div class="row">
                            <label class="col-sm-2 col-form-label">Nombre*:</label>
                            <div class="col-sm-10">
                            <input type="hidden" id="folio" value="<?php echo $folio ?>" name="folio">
                            <input type="text" class="form-control form-control-sm" id="nombre_1" name="nombre_1" required>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-2 col-form-label">Puesto Actual*:</label>
                            <div class="col-sm-10">
                            <input type="text" class="form-control form-control-sm" id="puesto_actual" name="puesto_actual" required>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-2 col-form-label">Experiencia en el puesto*:</label>
                            <div class="col-sm-4">
                            <input type="text" class="form-control form-control-sm" id="experiencia_puesto" name="experiencia_en_el_puesto" required>
                            </div>
                    
                            <label class="col-sm-2 col-form-label">Estado Civil:</label>
                            <div class="col-sm-4">
                            <input type="text" class="form-control form-control-sm" id="estado_civil" name="estado_civil">
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-2 col-form-label">Cumpleaños:</label>
                            <div class="col-sm-4">
                            <input type="date" class="form-control form-control-sm" id="cumpleanos" name="cumpleaños">
                            </div>
                    
                            <label class="col-sm-2 col-form-label">Aniversario:</label>
                            <div class="col-sm-4">
                            <input type="date" class="form-control form-control-sm" id="animersario" name="aniversario">
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-2 col-form-label">Teléfono:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control form-control-sm" id="telefono_1" name="telefono">
                            </div>
                    
                            <label class="col-sm-2 col-form-label">E-mail*:</label>
                            <div class="col-sm-4">
                                <input type="emal" class="form-control form-control-sm" id="email" name="email" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-control" style="background-color: #ced4da; margin-bottom: 10px">
                        <span><b>ll. Del la empresa</b></span>
                        <div class="row">
                            <label class="col-sm-2 col-form-label">Nombre:</label>
                            <div class="col-sm-10">
                            <input type="text" class="form-control form-control-sm" id="nombre_2" name="nombre_2">
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-2 col-form-label">Giro:</label>
                            <div class="col-sm-10">
                            <input type="text" class="form-control form-control-sm" id="giro" name="giro">
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-2 col-form-label">Dirección:</label>
                            <div class="col-sm-10">
                            <input type="text" class="form-control form-control-sm" id="direccion" name="direccion">
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-2 col-form-label">Teléfono:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control form-control-sm" id="telefono_2" name="telefono_2">
                            </div>
                    
                            <label class="col-sm-2 col-form-label">CP</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control form-control-sm" id="cp" name="CP">
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-2 col-form-label">E-mal:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control form-control-sm" id="email_2" name="email_2">
                            </div>                   
                            <label class="col-sm-2 col-form-label">Extensión:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control form-control-sm" id="extension" name="extension">
                            </div>
                        </div>
                    </div>
                    <div class="form-control" style="background-color: #ced4da; margin-bottom: 10px">
                        <span><b>lll. Del programa</b></span>
                        <div class="row">
                            <label class="col-sm-2 col-form-label">Nombre:</label>
                            <div class="col-sm-10">
                            <input type="text" class="form-control form-control-sm" id="nombre_3" name="nombre_3">
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-2 col-form-label">Inicio:</label>
                            <div class="col-sm-10">
                            <input type="text" class="form-control form-control-sm" id="inicio" name="inicio">
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-12 col-form-label">Razones por las que te interesó y expectativas:</label>
                            <div class="col-sm-12">
                            <input type="text" class="form-control form-control-sm" id="razones" name="razones_y_expectativas">
                            </div>
                        </div>
                    </div>
                    <div class="form-control" style="background-color: #ced4da; margin-bottom: 10px">
                        <span><b>lV. De F360°</b></span>
                        <div class="row">
                            <label class="col-sm-5 col-form-label">1. ¿Ha tenido la experiencia de trabajar con nostros?:</label>
                            <div class="col-sm-7">
                            <input type="text" class="form-control form-control-sm" id="pregunta_1" name="pregunta1">
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-5 col-form-label">2. ¿A través de qué medio se enteró de nostros?:</label>
                            <div class="col-sm-7">
                            <input type="text" class="form-control form-control-sm" id="pregunta_2" name="pregunta2">
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-5 col-form-label">3. ¿Le gustaría recibir información  de F360°?</label>
                            <div class="col-sm-7">
                            <input type="text" class="form-control form-control-sm" id="pregunta_3" name="pregunta3">
                            </div>
                        </div>
                    </div>
                    <div class="form-control" style="background-color: #ced4da; margin-bottom: 10px">
                        <span><b>V. Datos de facturación</b></span>
                        <div class="row">
                            <label class="col-sm-2 col-form-label">Razón social:</label>
                            <div class="col-sm-10">
                            <input type="text" class="form-control form-control-sm" id="razon_social" name="razon_social">
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-2 col-form-label">Domicilio fiscal:</label>
                            <div class="col-sm-10">
                            <input type="text" class="form-control form-control-sm" id="domicilio_fiscal" name="domicilio_fiscal">
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-2 col-form-label">Colonia:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control form-control-sm" id="colonia" name="colonia">
                            </div>
                    
                            <label class="col-sm-2 col-form-label">C.P:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control form-control-sm" id="cp_2" name="cp_2">
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-2 col-form-label">RFC:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control form-control-sm" id="rfc" name="rfc">
                            </div>
                    
                            <label class="col-sm-2 col-form-label">Municipio:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control form-control-sm" id="municipio" name="municipio">
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-2 col-form-label">Uso de CFDI:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control form-control-sm" id="uso_de_CFDI" name="uso_de_CFDI">
                            </div>
                    
                            <label class="col-sm-2 col-form-label">Método de pago:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control form-control-sm" id="metodo_de_pago" name="metodo_de_pago">
                            </div>
                        </div>
                    </div>
                
            </div>

            <div class="col-md-6 offset-md-6 text-right">
                <button class="btn btn-danger" onclick="location.href='https://www.f360.com.mx'"><i class="fas fa-trash"></i>  Cancelar</a></button>
                <!-- <button class="btn btn-info" onclick="location.href='solicitud_de_ingreso.php'"><i class="fas fa-sync"></i> Limpiar</button> -->
                <button type="submit" class="btn btn-success" id="btn_siguiente">Siguiente <i class="fas fa-arrow-circle-right"></i></button>
            </div> 
            </form>
        </div>
    </div><br><br>
    <?php include "footer.php" ?>
    <script src="js/formulario_de_ingreso.js"></script>
</body>
</html>