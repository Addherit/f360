<?php 
    include "../conexion.php";

    $email = $_POST['email'];
    $tipo_servicio = $_POST['tipo_servicio'];
    $fecha = date("Y-m-d H:i:s");

    
    $sql = "INSERT INTO solicitud_de_servicios (FechaSolicitud, Estatus, Email, TipoServicio) VALUES (?, ?, ?, ?)";
    $stmt = $conn -> prepare($sql);
    $stmt->execute([$fecha, 'PENDIENTE', $email, $tipo_servicio]);

    if($stmt){
        $sql = "SELECT * FROM solicitud_de_servicios WHERE Email = ? ";
        $stmt = $conn -> prepare($sql);
        $stmt->execute([$email]);
        foreach ($stmt as $row) { 
            $id_solicitud = $row ['IDsolicitud'];
        }

        $link = "https://www.addherit.com/f360/formulario_de_ingreso.php?id=".$id_solicitud;
        $sql = "UPDATE solicitud_de_servicios SET Link = ? WHERE IDsolicitud = '$id_solicitud'";
        $stmt = $conn -> prepare($sql);
        $stmt->execute([$link]);
        $validar_update = $stmt -> rowCount();

        if ($validar_update) { 

            $para = "$email";
            $asunto = "Formulario para encuesta F-360°";
            $mensaje = 
            
            '<html lang="en">
                <head>
                    <meta charset="UTF-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <meta http-equiv="X-UA-Compatible" content="ie=edge">    
                    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">            
                    <title>Document</title>
                </head>
                <body>    
                    <div class="container">       
                        <br>
                        <div class="card mb-4" style="border-top: 5px solid #e31f1f;">
                            <div class="card-body">      
                            <h3 class="text-center"><b>Gracias por tú participación</b></h3>  
                            <div class="col-12 text-center">
                            <img class="mx-auto mt-4 mb-3" width="200" src="https://www.f360.com.mx/wp-content/uploads/2019/02/7cbc92_b6789845a085421cb08c1e8b526fc926_mv2.png" />
                            </div> 
                                    
                            <hr/>
                
                            <h5 class="text-center"><strong>Instrucciones</strong></h5>
                            <p>Le invitamos a responder el siguiente formulario y el organigrama con respecto a su equipo a cargo</p>            
                            <strong>El link del formulario es el siguiente: </strong> <span><a href="'.$link.'">Formulario de ingreso</a></span>
                            </div>
                        </div>
                        <br><br>
                        <div class="text-center text-muted">
                            <p>© F360° 2019</p>    
                            <span>Av. Mariano Otero No. 1105, Primer piso</span><br>
                            <span>Col. Rinconadas del bosque</span><br>
                            <span>Guadalajara, Jalisco</span>
                        </div>
                
                        </div>
                    </div>                
                </body>
            </html>'
            
            ;

            $cabeceras = 'MIME-Version: 1.0' . "\r\n";
            $cabeceras .= 'Content-type: text/html; charset=utf-8' . "\r\n";
            $cabeceras .= 'From: Formulario para el participante<'.$email.'>';
                    
        
            $success_correo = mail($para, $asunto, $mensaje, $cabeceras);
            
            if (!$success_correo) {                
                echo "<div class='row' style='background-color: #f9a8a8; color: #ad0b0b; padding-top: 10px; padding-bottom: 10px; border-radius: 5px; margin: 0px; margin-bottom: 5px'><div class='col-sm-12'><span><b>ERROR</b> al intentar mandar el correo electrónico</span></div></div>";
            } else {                
                echo "<div class='row' style='background-color: #bddcbd; color: green; padding-top: 10px; padding-bottom: 10px; border-radius: 5px; margin: 0px; margin-bottom: 5px'><div class='col-sm-12'><span>Se envió el correo electrónico y se guardo el registro con éxito</span></div></div>";
            }
            

            //CREAR EL REGISTRO DEL FORMULARIO CON CORREO ELECTRONICO PRECARGADO
            $sql = "INSERT INTO solicitud_de_servicios_formulario (Email_1, IDsolicitud) VALUES(?,?)";
            $stmt = $conn -> prepare($sql);
            $stmt->execute([$email, $id_solicitud]);

            //validar que se haya creado y mandar correo con la liga correcta 

        } else {
            echo 'ocurrio un error al crear un link para el acceso al formulario';
        }
    } else {
        echo 'ocurrio un error al intentar registrar un usuario que solicitó el servicio';
    }

    $stmt->closeCursor(); // opcional en MySQL, dependiendo del controlador de base de datos puede ser obligatorio
    $stmt = null; // obligado para cerrar la conexión
    $pdo = null;
?>