<?php 

    include "../conexion.php";

    $id_solicitud = $_POST['id_solicitud'];

    if (isset($_POST['naturaleza_actual'])) {
        $naturaleza_actual = $_POST['naturaleza_actual'];
        $sql = "SELECT preguntas.Id_pregunta, Pregunta, Id_participante, Nombre_naturaleza, Competencia_asignada, Descripcion_competencia, Es_lider, Respuesta FROM catalogo_encuestas_preguntas AS preguntas 
        LEFT JOIN catalogo_encuestas_respuestas AS respuestas ON preguntas.Id_pregunta = respuestas.Id_pregunta 
        LEFT JOIN equipo_organigrama AS equipo ON respuestas.Id_participante = equipo.Nombre 
        LEFT JOIN competencias AS competencias ON preguntas.Competencia_asignada = competencias.Nombre_competencia 
        WHERE Id_solicitud_de_servicio = ? AND Nombre_naturaleza = ?";
        $stmt = $conn -> prepare ($sql);
        $stmt -> execute ([ $id_solicitud, $naturaleza_actual ]);
    } else {
        $sql = "SELECT preguntas.Id_pregunta, Pregunta, Id_participante, Nombre_naturaleza, Competencia_asignada, Descripcion_competencia, Es_lider, Respuesta FROM catalogo_encuestas_preguntas AS preguntas 
        LEFT JOIN catalogo_encuestas_respuestas AS respuestas ON preguntas.Id_pregunta = respuestas.Id_pregunta 
        LEFT JOIN equipo_organigrama AS equipo ON respuestas.Id_participante = equipo.Nombre 
        LEFT JOIN competencias AS competencias ON preguntas.Competencia_asignada = competencias.Nombre_competencia 
        WHERE Id_solicitud_de_servicio = ?";
        $stmt = $conn -> prepare ($sql);
        $stmt -> execute ([ $id_solicitud]);
    }

  

    $array_respuestas_generales = [];
    $es_lider = [];
    $equipo = [];

    foreach ($stmt as $row) {
        $linea = [
            'id_pregunta' => $row['Id_pregunta'],
            'pregunta' => $row['Pregunta'],
            'id_participante' => $row['Id_participante'],
            'nombre_naturaleza' => $row['Nombre_naturaleza'],
            'competencia_asignada' => $row['Competencia_asignada'],   
            'descripcion_competencia' => $row['Descripcion_competencia'],   
            'respuesta' => $row['Respuesta'],
            'es_lider' => $row['Es_lider'],
        ];

        if ($row['Es_lider'] > 0) {                
            array_push($es_lider, $linea);             
        } else {
            array_push($equipo, $linea);             
        }
                   
    }
    array_push($array_respuestas_generales, $es_lider, $equipo);
    echo json_encode($array_respuestas_generales);

    $stmt = null;
    $conn = null;
?>