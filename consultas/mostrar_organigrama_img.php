    <?php
        include "../conexion.php";

        $id_solicitud = $_POST['id'];

        $sql = "SELECT * FROM solicitud_de_servicios WHERE IDsolicitud = ?";
        $stmt = $conn -> prepare ($sql);
        $stmt -> execute([$id_solicitud]);

        $array_respuesta = [];

        foreach ($stmt as $row) {

            array_push($array_respuesta, $row['NombreSolicitante'], $row['Email'], $row['Puesto']);            
        }

        echo json_encode($array_respuesta)
    
    ?>