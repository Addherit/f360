<?php 
    include "../conexion.php";
    
    $cantidad_de_competencias = $_POST['num_competencia_modal'];
    $id_encuesta = $_POST['competencias_para_id_encuesta'];
     
    $estatus = 'ACTIVO';

    for ($x=1; $cantidad_de_competencias >= $x ; $x++) { 
        
        $nombre_competencia = $_POST['nombre_competencia_'.$x];
        $descripcion_competencia = $_POST['descripcion_competencia_'.$x];

        $sql = "INSERT INTO competencias (Id_encuesta, Nombre_competencia, Descripcion_competencia, Estatus) VALUES (?,?,?,?)";
        $stmt = $conn -> prepare($sql);
        $stmt -> execute([ $id_encuesta, $nombre_competencia, $descripcion_competencia, $estatus]);
        $validador = $stmt -> rowCount();
        if ($validador) {
            echo "<div class='row' style='background-color: #bddcbd; color: green; padding-top: 10px; padding-bottom: 10px; border-radius: 5px; margin: 0px; margin-bottom: 5px'><div class='col-sm-12'><span>Competencia guardada con <b>éxito</b></span></div></div>";
        } else {
            echo "<div class='row' style='background-color: #f9a8a8; color: #ad0b0b; padding-top: 10px; padding-bottom: 10px; border-radius: 5px; margin: 0px; margin-bottom: 5px'><div class='col-sm-12'><span><b>ERROR</b> al crear compentencia, favor de ponerse en contacto con sistemas</span></div></div>";
        }    
    }
    
    $pdo = null;
?>