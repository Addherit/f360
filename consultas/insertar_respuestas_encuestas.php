<?php 
    include "../conexion.php";

    $id_encuesta = $_POST['id_encuesta_para_contestar'];
    $id_servicio_solicitado = $_POST['id_servicio_solicitado'];
    $persona_que_contesta = $_POST['persona_asignada_en_link'];
    $total_respuestas = $_POST['total_respuestas'];
    
    //sacar los numero de las preguntas contestadas
    $array_num_resp = [];
    for ($i=1;  $total_respuestas >= $i; $i++) {         
        $num_resp = $_POST['num_resp_'.$i];
        array_push($array_num_resp, $num_resp);
    }
    
    for ($x=0; $total_respuestas > $x ; $x++) { 
                        
        $num_resp = $array_num_resp[$x];        
        $respuesta = $_POST['respuesta_num_'.$num_resp];        

        $sql ="INSERT INTO catalogo_encuestas_respuestas (Id_encuesta, Id_pregunta, Id_participante, Id_solicitud_de_servicio, Respuesta) VALUES (?,?,?,?)";
        $stmt = $conn -> prepare($sql);
        $stmt -> execute([$id_encuesta, $num_resp , $persona_que_contesta, $id_servicio_solicitado, $respuesta ]);       
    }    

    $validar = $stmt -> rowCount();

    if ($validar) {
        

        //cambiar el YA_CONTESTO
        $sql_ya_contesto = "UPDATE equipo_organigrama SET Ya_contesto = '1' WHERE Nombre = ? AND Id_folio_servicio_solicitado = ?";
        $stmt = $conn -> prepare($sql_ya_contesto);
        $stmt -> execute([$persona_que_contesta, $id_servicio_solicitado ]);

  
        $sql = "SELECT * FROM equipo_organigrama WHERE Id_folio_servicio_solicitado = ? AND Ya_contesto = ?";
        $stmt = $conn -> prepare($sql);
        $stmt -> execute([$id_servicio_solicitado, 0]);

        $validar = $stmt -> rowCount();

        if ($validar) {
           echo "aun hay personas sin contestar la encuesta";
        } else {
            $sql ="UPDATE solicitud_De_servicios SET Estatus = ? WHERE IDsolicitud = ? ";
            $stmt = $conn -> prepare($sql);
            $stmt -> execute(['LISTO PARA REPORTEO', $id_servicio_solicitado ]);   
        }

        echo "Se guardo con éxito las respuestas, GRACIAS";

    } else {
        echo "Algo salió mal al intentar guardar sus respuestas, favor de ponerse en contacto con nosotros";
    }

?>