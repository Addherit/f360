<?php
    include "../conexion.php";

    $id_pregunta = $_POST['id_pregunta'];
    $sql ="SELECT * FROM opcion_multiple WHERE Id_pregunta = ?";
    $stmt = $conn -> prepare ($sql);
    $stmt -> execute ([$id_pregunta]);

    $respuesta_array = [];

    foreach ($stmt as $row) {
        $linea = [
            "id_pregunta" => $row['Id_pregunta'],
            "opcion_multiple" => $row['Opcion_multiple'],
        ];
        array_push($respuesta_array, $linea);
    }
     echo json_encode($respuesta_array);

?>