<?php 
    include "../conexion.php";

    if ($_POST['id_encuesta'] == 0) {
        $sql = "SELECT * FROM catalogo_encuestas order by Id_encuesta desc"; 
    } else {
        $id_encuesta = $_POST['id_encuesta'];
        $sql = "SELECT * FROM catalogo_encuestas WHERE Id_encuesta = '$id_encuesta'";
    }

     
    $stmt = $conn -> query($sql);
    $respuesta_array = [];

    if ($stmt) {
        
        foreach ($stmt as $row) {
            $linea = [
                "id_encuesta" => $row["Id_encuesta"],
                "titulo_encuesta" => $row["Titulo_encuesta"],
                "tipo_encuesta" => $row["Tipo_encuesta"],
                "fecha_creacion" => $row["Fecha_creacion"],
                "estatus" => $row["Estatus"],

            ];  
            array_push($respuesta_array, $linea);
        }
        echo json_encode($respuesta_array); 
    }

    
    
    $stmt->closeCursor(); // opcional en MySQL, dependiendo del controlador de base de datos puede ser obligatorio
    $stmt = null; // obligado para cerrar la conexión
    $pdo = null;
?>