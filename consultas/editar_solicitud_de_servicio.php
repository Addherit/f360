<?php 
    include "../conexion.php";

    $id = $_POST['id_solicitud_servicio_modal_hide'];
    // $nombre = $_POST["nombre_notificacion_servicio_editar"];
    // $correo = $_POST['correo_notificacion_servicio_editar'];
    $encuesta = $_POST["slt_encuestas_disponibles_modal_editar"];    
    $estatus = "ENCUESTA ASIGNADA";

    $sql = "UPDATE solicitud_de_servicios SET Encuesta_asignada = ?, Estatus = ? WHERE IDsolicitud = ? ";
    $stmt = $conn -> prepare ($sql);
    $stmt -> execute([$encuesta, $estatus, $id]);
    $validador = $stmt -> rowCount();

    if ($validador) {
        echo "<div class='row' style='background-color: #bddcbd; color: green; padding-top: 10px; padding-bottom: 10px; border-radius: 5px; margin: 0px; margin-bottom: 5px'><div class='col-sm-12'><span>Se asignó encuesta con <b>éxito</b></span></div></div>";        
    } else {
        echo "<div class='row' style='background-color: #f9a8a8; color: #ad0b0b; padding-top: 10px; padding-bottom: 10px; border-radius: 5px; margin: 0px; margin-bottom: 5px'><div class='col-sm-12'><span><b>ERROR</b> al asignar encuesta, favor de ponerse en contacto con sistemas</span></div></div>";
    }
    $pdo = null;
?>