<?php    
    include "../conexion.php";  

    $cuenta = $_POST['email'];
    $pass = $_POST['pass'];

    $sql = "SELECT Nombres, Apellidos FROM usuarios WHERE Email = ? AND Password = ? ";
    $stmt = $conn->prepare($sql);
    $stmt->execute([$cuenta, $pass]);
    $resp = 0;

    if($resp=$stmt -> rowCount()){
        foreach($stmt as $row) {
            $nombre = $row["Nombres"];
            $apellidos = $row["Apellidos"];        
        }
        $array = array(            
            "nombre" => $nombre,
            "apellidos" => $apellidos,             
        );
        echo json_encode($array);

         session_start();
         $_SESSION["nombreUsuario"] = $nombre;
         $_SESSION["apellidosUsuario"] = $apellidos;        
    } else {
        echo $resp;
    }

    $stmt->closeCursor(); // opcional en MySQL, dependiendo del controlador de base de datos puede ser obligatorio
    $stmt = null; // obligado para cerrar la conexión
    $pdo = null;
?>