<?php 
    include "../conexion.php";

    $id_a_eliminar = $_POST['id_a_eliminar'];

    $sql = "DELETE FROM solicitud_de_Servicios WHERE IDsolicitud = ?";
    $stmt = $conn -> prepare($sql);
    $stmt -> execute([$id_a_eliminar]); 
    $validador = $stmt -> rowCount();

    if($validador){
        echo "<div class='row' style='background-color: #bddcbd; color: green; padding-top: 10px; padding-bottom: 10px; border-radius: 5px; margin: 0px; margin-bottom: 5px'><div class='col-sm-12'><span>Solicitud de servicios eliminada correctamente</span></div></div>";
    } else {
        echo "<div class='row' style='background-color: #f9a8a8; color: #ad0b0b; padding-top: 10px; padding-bottom: 10px; border-radius: 5px; margin: 0px; margin-bottom: 5px'><div class='col-sm-12'><span><b>ERROR</b> al tratar de eliminar solicitud de servicios, favor de ponerse en contacto con sistemas</span></div></div>";
    }
?>