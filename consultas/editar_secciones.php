<?php 
    include "../conexion.php";

    $id = $_POST['id_seccion_a_editar_modal_seccion_editar'];
    $tipo = $_POST["tipo_competencia_modal_seccion_editar"];
    $nombre = $_POST['nombre_seccion_modal_editar'];
    $estatus = $_POST["estatus_seccion_modal_editar"];    

    $sql = "UPDATE secciones SET Tipo_seccion = ?, Nombre_seccion = ?, Estatus = ? WHERE Id_seccion = ?";
    $stmt = $conn -> prepare ($sql);
    $stmt -> execute([$tipo, $nombre, $estatus, $id]);
    $validador = $stmt -> rowCount();

    if ($validador) {
        echo "<div class='row' style='background-color: #bddcbd; color: green; padding-top: 10px; padding-bottom: 10px; border-radius: 5px; margin: 0px; margin-bottom: 5px'><div class='col-sm-12'><span>Competencia editada con <b>éxito</b></span></div></div>";
    } else {
        echo "<div class='row' style='background-color: #f9a8a8; color: #ad0b0b; padding-top: 10px; padding-bottom: 10px; border-radius: 5px; margin: 0px; margin-bottom: 5px'><div class='col-sm-12'><span><b>ERROR</b> al ediatr compentencia, favor de ponerse en contacto con sistemas</span></div></div>";
    }
    $pdo = null;
?>