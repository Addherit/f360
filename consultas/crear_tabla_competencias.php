<?php 
    include "../conexion.php";

    $folio_servicio = $_POST['id'];

    //OBTENER ENCUESTA ASIGNADA
    $sql = "SELECT Encuesta_asignada, Titulo_encuesta FROM solicitud_de_servicios LEFT JOIN catalogo_encuestas ON Encuesta_asignada = Id_encuesta WHERE IDsolicitud = ?";
    $stmt = $conn -> prepare($sql);
    $stmt -> execute ([$folio_servicio]);
    foreach ($stmt as $row ) {
        $id_encuesta_asignada =  $row['Encuesta_asignada'];
        $titulo_encuesta =  $row['Titulo_encuesta'];
    }

    //NATURALEZAS
    $naturalezas = [];
    $sql = "SELECT DISTINCT Nombre_naturaleza FROM catalogo_encuestas_preguntas WHERE Id_encuesta = ?";
    $stmt = $conn -> prepare($sql);
    $stmt -> execute ([$id_encuesta_asignada]);

    foreach ($stmt as $row ){       
        array_push($naturalezas,  $row['Nombre_naturaleza']); 
    }

    // COMPETENCIAS - NATURALEZAS
    $competencia_dividida = [];
    for ($x=0; sizeof($naturalezas) > $x; $x++) { 

        $naturaleza_actual = $naturalezas[$x];
        $sql = "SELECT Competencia_asignada, Nombre_naturaleza, Descripcion_competencia 
        FROM catalogo_encuestas_preguntas AS cat
        LEFT JOIN competencias AS com ON Competencia_asignada = Nombre_competencia
        WHERE cat.Id_encuesta = ? AND Nombre_naturaleza = ? GROUP BY Competencia_asignada";
        $stmt = $conn -> prepare($sql);
        $stmt -> execute ([$id_encuesta_asignada, $naturaleza_actual]);
        $competencia_naturaleza = [];
        foreach ($stmt as $row) {
            $linea = [];   
            array_push($linea, $row['Competencia_asignada'], $row['Nombre_naturaleza'], $row['Descripcion_competencia']);
            array_push($competencia_naturaleza, $linea);
        }
        array_push($competencia_dividida, $competencia_naturaleza);
    }
    
    $array_de_arrays_mamalon = [];
    array_push($array_de_arrays_mamalon, $naturalezas, $competencia_dividida);
    echo json_encode($array_de_arrays_mamalon);

?>