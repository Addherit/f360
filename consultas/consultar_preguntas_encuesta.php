<?php 
    include "../conexion.php";
    $id_encuesta = $_POST['id_encuesta'];

    $sql = "SELECT * FROM catalogo_encuestas_preguntas WHERE Id_encuesta = ?";
    $stmt = $conn ->prepare($sql);
    $stmt -> execute([$id_encuesta]);
    $validar = $stmt -> rowCount();
    $respuesta_array = [];

    foreach ($stmt as $row) {               
        $linea = [
            "id_pregunta" => $row["Id_pregunta"],
            "id_encuesta" => $row["Id_encuesta"],
            "nombre_naturaleza" => $row["Nombre_naturaleza"],
            "pregunta" => $row["Pregunta"],
            "competencia_asignada" => $row["Competencia_asignada"],
            "tipo_pregunta" => $row["Tipo_pregunta"],
            "estatus" => $row["Estatus"],
        ];  
        array_push($respuesta_array, $linea);
    }
    echo json_encode($respuesta_array); 

?>