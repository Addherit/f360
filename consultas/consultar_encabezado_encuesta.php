<?php
    include "../conexion.php";

    $id_encuesta = $_POST['id_encuesta'];

    $sql = "SELECT * FROM catalogo_encuestas WHERE Id_encuesta = ?";
    $stmt = $conn ->prepare($sql);
    $stmt -> execute([$id_encuesta]);
    $validar = $stmt -> rowCount();
    $respuesta_array = [];

    foreach ($stmt as $row) {               
        $linea = [
            "id_encuesta" => $row["Id_encuesta"],
            "titulo_encuesta" => $row["Titulo_encuesta"],
            "fecha_creacion" => $row["Fecha_creacion"],
            "tipo_encuesta" => $row["Tipo_encuesta"],
            "estatus" => $row["Estatus"],          
        ];  
        array_push($respuesta_array, $linea);
    }
    echo json_encode($respuesta_array); 
?>