<?php 
    include "../conexion.php";


    $tipo_seccion = $_POST['tipo_seccion'];
    $nombre_seccion = $_POST['nombre_seccion'];    
    $estatus = 'ACTIVO';

    $sql = "INSERT INTO secciones (Tipo_seccion, Nombre_seccion, Estatus) VALUES (?,?,?)";
    $stmt = $conn -> prepare($sql);
    $stmt -> execute([$tipo_seccion, $nombre_seccion, $estatus]);
    $validador = $stmt -> rowCount();

    if ($validador) {
        echo "<div class='row' style='background-color: #bddcbd; color: green; padding-top: 10px; padding-bottom: 10px; border-radius: 5px; margin: 0px; margin-bottom: 5px'><div class='col-sm-12'><span>sección guardada con <b>éxito</b></span></div></div>";
    } else {
        echo "<div class='row' style='background-color: #f9a8a8; color: #ad0b0b; padding-top: 10px; padding-bottom: 10px; border-radius: 5px; margin: 0px; margin-bottom: 5px'><div class='col-sm-12'><span><b>ERROR</b> al crear sección, favor de ponerse en contacto con sistemas</span></div></div>";
    }
    $pdo = null;
?>