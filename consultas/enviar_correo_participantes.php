<?php 
    include "../conexion.php";
    $id_solicitud_servicio = $_POST['id_solicitud_servicio'];

    $sql = "SELECT * FROM equipo_organigrama 
            LEFT JOIN solicitud_de_servicios ON IDsolicitado = Id_folio_servicio_solicitado 
            WHERE Id_folio_servicio_solicitado = ?";
    $stmt = $conn -> prepare($sql);
    $stmt -> execute([$id_solicitud_servicio]);

    foreach ($stmt as $row) {
        if ($row['Es_lider'] > 0) {
           $es_lider = $row['Nombre'];
           $id_encuesta_asignada = $row['Encuesta_asignada'];
        } 
    } 

 

    //sigle y participantes
    $sql = "SELECT * FROM equipo_organigrama WHERE Id_folio_servicio_solicitado = ?";
    $stmt = $conn -> prepare($sql);
    $stmt -> execute([$id_solicitud_servicio]);

    foreach ($stmt as $row) {
        $email = $row['Email'];
        $link ='https://www.addherit.com/f360/contestar_encuesta.php?id_servicio='.$row['Id_folio_servicio_solicitado'].'&persona_asignada='.$row['Nombre'].'&id_encuesta='.$id_encuesta_asignada;
        
        $para = "$email";
        $asunto = "Encuesta F 360°";

        $cabeceras = 'MIME-Version: 1.0' . "\r\n";
        $cabeceras .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        $cabeceras .= 'From: Formulario para el participante<'.$email.'>';

      

        if ($row['Es_lider'] > 0) {            
            $cuerpito = 'Hola, '.$row['Nombre'].' esperamos que te encuentres muy bien. Te mandamos la encuesta para tu autoevaluación, contesta de aucerdo a tu criterio. Gracias.'; 
        } else {
            $cuerpito = 'Hola, '.$row['Nombre'].' esperamos que te encuentres muy bien. Te mandamos la encuesta para evaluar al participante '.$es_lider.', contesta de aucerdo a tu criterio. Gracias.'; 
        }

        $mensaje = '<html lang="en">
            <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <meta http-equiv="X-UA-Compatible" content="ie=edge">    
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">            
                <title>Document</title>
            </head>
            <body>    
                <div class="container">       
                    <br>
                    <div class="card mb-4" style="border-top: 5px solid #e31f1f;">
                        <div class="card-body">      
                        <h3 class="text-center"><b>Gracias por tú participación</b></h3>  
                        <div class="col-12 text-center">
                        <img class="mx-auto mt-4 mb-3" width="200" src="https://www.f360.com.mx/wp-content/uploads/2019/02/7cbc92_b6789845a085421cb08c1e8b526fc926_mv2.png" />
                        </div> 
                                
                        <hr/>
            
                        <h5 class="text-center"><strong>Instrucciones</strong></h5>
                        <p>'.$cuerpito.'</p>            
                        <strong>El link del formulario es el siguiente: </strong> <span><a href="'.$link.'">Formulario de ingreso</a></span>
                        </div>
                    </div>
                    <br><br>
                    <div class="text-center text-muted">
                        <p>© F360° 2019</p>    
                        <span>Av. Mariano Otero No. 1105, Primer piso</span><br>
                        <span>Col. Rinconadas del bosque</span><br>
                        <span>Guadalajara, Jalisco</span>
                    </div>
            
                    </div>
                </div>                
            </body>
        </html>';
        
    
        $success_correo = mail($para, $asunto, $mensaje, $cabeceras);

        $sql_update_url = "UPDATE equipo_organigrama SET Url_correo_encuesta = ? WHERE Id_folio_servicio_solicitado = ? AND Nombre = ? ";
        $stmt = $conn -> prepare($sql_update_url);
        $stmt -> execute([$link, $id_solicitud_servicio, $row['Nombre']]);


        
    }

    $validar = $stmt -> rowCount();

    if ($validar) {
        echo "Se asigno la encuesta al equipo.";
    } else {
        echo "No se pudo envíar la encuesta al equipo.";
    }

?>