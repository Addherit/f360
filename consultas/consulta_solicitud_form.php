<?php 
    include "../conexion.php";

    $folio = $_POST['folio'];

    $sql = "SELECT * FROM solicitud_de_servicios_formulario WHERE IDsolicitud = ?";
    $stmt = $conn -> prepare($sql);
    $stmt -> execute([$folio]);
    
    $respuesta_array = [];

    foreach ($stmt as $row) {
        $respuesta_array = [
            "id_solicitud" => $row['IDsolicitud'],
            "nombre_1" => $row['Nombre_1'],
            "puesto_actual" => $row['PuestoActual'],
            "experiencia_puesto" => $row['ExperienciaPuesto'],
            "estado_civil" => $row['EstadoCivil'],
            "cumpleanos" => $row['Cumpleanos'],
            "aniversario" => $row['Aniversario'],
            "telefono_1" => $row['Telefono_1'],
            "email" => $row['Email_1'],
            "nombre_2" => $row['Nombre_2'],
            "giro" => $row['Giro'],
            "direccion" => $row['Direccion'],
            "telefono_2" => $row['Telefono_2'],
            "cp" => $row['CP'],
            "email_2" => $row['Email_2'],
            "extension" => $row['Extension'],
            "nombre_3" => $row['Nombre_3'],
            "inicio" => $row['Inicio'],
            "razones" => $row['Razones'],
            "pregunta_1" => $row['Pregunta_1'],
            "pregunta_2" => $row['Pregunta_2'],
            "pregunta_3" => $row['Pregunta_3'],
            "razon_social" => $row['RazonSocial'],
            "domicilio_fiscal" => $row['DomicilioFiscal'],
            "colonia" => $row['Colonia'],
            "cp_2" => $row['CP_2'],
            "rfc" => $row['RFC'],
            "municipio" => $row['Municipio'],
            "uso_de_cfdi" => $row['UsoDeCFDI'],
            "metodo_de_pago" => $row['MetodoDePago'],
        ];
    }

    echo json_encode($respuesta_array);


    $stmt = null;
    $conn = null;
?>