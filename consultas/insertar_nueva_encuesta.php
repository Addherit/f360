<?php 
    include "../conexion.php";
    $id_encuesta = $_POST['id_encuesta'];
    $num_preguntas = $_POST['num_preguntas'];
    $titulo_encuesta = $_POST['titulo_encuesta'];
    $fecha_hoy = date("d-m-Y");
    $tipo_encuesta = 1;
    $estatus = 'ABIERTO';

    $array_total_preguntas = [];
    $resp="";

    for($x=1; $num_preguntas >= $x; $x++ ) {
        
        $array_pregunta_completa = [];

        //PREGUNTAS // pregunta_num_X
        $pregunta_[$x] = $_POST['pregunta_num_'.$x];

        //TIPO DE PREGUNTA // slt_pregunta_x
        $tipo_pregunta = $_POST['slt_pregunta_'.$x];

        //COMPETENCIA ASIGNADA A PREGUNTA // slt_pregunta_competencia_x
        $competencia_pregunta = $_POST['slt_pregunta_competencia_'.$x];

        //NATURALEZA ASIGNADA A PREGUNTA // naturaleza_pregunta_x
        $naturaleza_pregunta = $_POST['naturaleza_pregunta_'.$x];

        if ($tipo_pregunta == 'multiple') {
            
            //OPCIONES MULTIPLES // input_num_consecutivo_pregunta_num_x
            $num_opciones_multiples = $_POST['input_num_consecutivo_pregunta_num_'.$x];
            $opciones_multiples = [];

            for ($y=1; $num_opciones_multiples >= $y; $y++) { 

                // opcion_1_pregunta_y
                $opcion_[$y] = $_POST['opcion_'.$y.'_pregunta_'.$x];
                array_push($opciones_multiples, $opcion_[$y]);
            }
            array_push($array_pregunta_completa, $pregunta_[$x], $competencia_pregunta, $tipo_pregunta, $opciones_multiples);
            array_push($array_total_preguntas, $array_pregunta_completa);
            
        } else {
            array_push($array_pregunta_completa, $pregunta_[$x], $competencia_pregunta, $tipo_pregunta, $naturaleza_pregunta);
            array_push($array_total_preguntas, $array_pregunta_completa);
        }

    } // se hace el array de todas las preguntas

    //INSERT DEL ENCABEZADO DE LA ENCUESTA
    $sql="INSERT INTO catalogo_encuestas (Id_encuesta, Titulo_encuesta, Fecha_creacion, Tipo_encuesta, Estatus) VALUES (?,?, NOW(),?,?)";
    $stmt = $conn -> prepare($sql);
    $stmt->execute([$id_encuesta, $titulo_encuesta, $tipo_encuesta , $estatus ]);
    $validador = $stmt->rowCount();

    if ($validador) {

        // AQUI SE INSERTARAN LAS PREGUNTAS RECORRIENDO EL ARRAY
        for ($i=0; $num_preguntas > $i ; $i++) { 
            
            
        
            $sql="INSERT INTO catalogo_encuestas_preguntas (Id_encuesta, Nombre_naturaleza, Pregunta, Competencia_asignada, Tipo_pregunta, Estatus) VALUES (?,?,?,?,?,?)";
            $stmt = $conn -> prepare($sql);
            $stmt -> execute([$id_encuesta, $array_total_preguntas[$i][3], $array_total_preguntas[$i][0], $array_total_preguntas[$i][1], $array_total_preguntas[$i][2], 'ACTIVO']);        
            $validador = $stmt->rowCount();

            if ($validador) {
                //SI ES OPCION MULTIPLE AQUI SE GUARDAN 
                if ($array_total_preguntas[$i][1] == 'multiple') {

                    $ultima_pregunta = $conn -> query("SELECT Id_pregunta FROM catalogo_encuestas_preguntas ORDER BY Id_pregunta DESC LIMIT 1") ->fetch();            

                    for ($opcion_multiple=0; sizeof($array_total_preguntas[$i][2]) > $opcion_multiple ; $opcion_multiple++) { 

                        $sql="INSERT INTO opcion_multiple (Id_pregunta, Opcion_multiple) VALUES (?,?)";
                        $stmt = $conn -> prepare($sql);
                        $stmt -> execute([$ultima_pregunta['Id_pregunta'], $array_total_preguntas[$i][2][$opcion_multiple]]);  

                    }
                
                }        
            } else{
                echo $resp = "<div class='row' style='background-color: #f9a8a8; color: #ad0b0b; padding-top: 10px; padding-bottom: 10px; border-radius: 5px; margin: 0px; margin-bottom: 5px'><div class='col-sm-12'><span><b>ERROR</b> al guardar las preguntas de la encuesta, favor de ponerse en contacto con sistemas</span></div></div>";
            }

            
        }
        echo $resp = "<div class='row' style='background-color: #bddcbd; color: green; padding-top: 10px; padding-bottom: 10px; border-radius: 5px; margin: 0px; margin-bottom: 5px'><div class='col-sm-12'><span>Encuesta guardada con <b>éxito</b></span></div></div>";
    } else {
        echo $resp = "<div class='row' style='background-color: #f9a8a8; color: #ad0b0b; padding-top: 10px; padding-bottom: 10px; border-radius: 5px; margin: 0px; margin-bottom: 5px'><div class='col-sm-12'><span><b>ERROR</b> al guardar el encabezado de la encuesta, favor de ponerse en contacto con sistemas</span></div></div>";
    }

    $pdo = null;
    
?>