<?php 
    include "../conexion.php";
    $respuesta = [];
    $folio = $_POST['folio'];
    $nombre_1 = $_POST['nombre_1'];
    $puesto_actual = $_POST['puesto_actual'];
    $experiencia_en_el_puesto = $_POST['experiencia_en_el_puesto'];
    $estado_civil = $_POST['estado_civil'];
    $cumpleaños = $_POST['cumpleaños'];
    $aniversario = $_POST['aniversario'];
    $telefono = $_POST['telefono'];
    $email = $_POST['email'];
    $nombre_2 = $_POST['nombre_2'];
    $giro = $_POST['giro'];
    $direccion = $_POST['direccion'];
    $telefono_2 = $_POST['telefono_2'];
    $CP = $_POST['CP'];
    $email_2 = $_POST['email_2'];
    $extension = $_POST['extension'];
    $nombre_3 = $_POST['nombre_3'];
    $inicio = $_POST['inicio'];
    $razones_y_expectativas = $_POST['razones_y_expectativas'];
    $pregunta1 = $_POST['pregunta1'];
    $pregunta2 = $_POST['pregunta2'];
    $pregunta3 = $_POST['pregunta3'];
    $razon_social = $_POST['razon_social'];
    $domicilio_fiscal = $_POST['domicilio_fiscal'];
    $colonia = $_POST['colonia'];
    $cp_2 = $_POST['cp_2'];
    $rfc = $_POST['rfc'];
    $monicipio = $_POST['municipio'];
    $uso_de_CFDI = $_POST['uso_de_CFDI'];
    $metodo_de_pago = $_POST['metodo_de_pago'];    


    // validar si existe ya el registro
    $sql = "SELECT * FROM solicitud_de_servicios_formulario WHERE IDsolicitud = ?";
    $stmt = $conn -> prepare($sql);
    $stmt -> execute([$folio]);
    $cuenta = $stmt->rowCount();

    
    if($cuenta) {
        $sql = "UPDATE solicitud_de_servicios_formulario SET Nombre_1 = ?, PuestoActual = ?, ExperienciaPuesto = ?, EstadoCivil = ?, Cumpleanos = ?, Aniversario = ?, Telefono_1 = ?, Email_1 = ?, Nombre_2 = ?, Giro = ?,
        Direccion = ?, Telefono_2 = ?, CP = ?, Email_2 = ?, Extension = ?, Nombre_3 = ?, Inicio = ?, Razones = ?, Pregunta_1 = ?, Pregunta_2 = ?, Pregunta_3 = ?, RazonSocial = ?, DomicilioFiscal = ?,
        Colonia = ?, CP_2 = ?, RFC = ?, Municipio = ?, UsoDeCFDI = ?, MetodoDePago = ? WHERE IDsolicitud = ?";

    } else {
        $sql = "INSERT INTO solicitud_de_servicios_formulario 
        (Nombre_1, PuestoActual, ExperienciaPuesto, EstadoCivil, Cumpleanos, Aniversario, Telefono_1, Email_1, Nombre_2, Giro,
        Direccion, Telefono_2, CP, Email_2, Extension, Nombre_3, Inicio, Razones, Pregunta_1, Pregunta_2, Pregunta_3, RazonSocial, DomicilioFiscal,
        Colonia, CP_2, RFC, Municipio, UsoDeCFDI, MetodoDePago, IDsolicitud) 
        VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    }

    $stmt = $conn -> prepare($sql);

    $stmt->execute([$nombre_1, $puesto_actual, $experiencia_en_el_puesto, $estado_civil, $cumpleaños, $aniversario, $telefono,
    $email, $nombre_2, $giro, $direccion, $telefono_2, $CP, $email_2, $extension, $nombre_3, $inicio, $razones_y_expectativas,
    $pregunta1, $pregunta2, $pregunta3, $razon_social, $domicilio_fiscal, $colonia, $cp_2, $rfc, $monicipio, $uso_de_CFDI, $metodo_de_pago, $folio]);
    
    $validar_sql_accion = $stmt -> rowCount();
    // if ($validar_sql_accion) { 

        $sql = "UPDATE solicitud_de_servicios SET CheckForm = 2, Estatus = 'FALTA ORGANIGRAMA', NombreSolicitante = '$nombre_1' WHERE IDsolicitud = ?";
        $stmt = $conn -> prepare ($sql);
        $stmt -> execute([$folio]);
        
        array_push($respuesta, "listo", $nombre_1, $email, $validar_sql_accion);

    // } else {
    //     array_push($respuesta, "nada", $stmt);
    // }

    echo json_encode( $respuesta);

    $stmt->closeCursor(); // opcional en MySQL, dependiendo del controlador de base de datos puede ser obligatorio
    $stmt = null; // obligado para cerrar la conexión
    $pdo = null;

?>