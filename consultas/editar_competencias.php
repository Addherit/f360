<?php 
    include "../conexion.php";

    $nombre = $_POST["nombre_competencia_modal_editar"];
    $estatus = $_POST["estatus_competencia_modal_editar"];
    $id = $_POST["id_competencia_a_editar_modal_editar"];
    $descripcion = $_POST["descripcion_textarea_competencia"];

    $sql = "UPDATE competencias SET Nombre_competencia = ?, Estatus = ?, Descripcion_competencia = ? WHERE Id_competencia = ?";
    $stmt = $conn -> prepare ($sql);
    $stmt -> execute([$nombre, $estatus, $descripcion, $id]);
    $validador = $stmt -> rowCount();

    if ($validador) {
        echo "<div class='row' style='background-color: #bddcbd; color: green; padding-top: 10px; padding-bottom: 10px; border-radius: 5px; margin: 0px; margin-bottom: 5px'><div class='col-sm-12'><span>Competencia editada con <b>éxito</b></span></div></div>";
    } else {
        echo "<div class='row' style='background-color: #f9a8a8; color: #ad0b0b; padding-top: 10px; padding-bottom: 10px; border-radius: 5px; margin: 0px; margin-bottom: 5px'><div class='col-sm-12'><span><b>ERROR</b> al editar compentencia, favor de ponerse en contacto con sistemas</span></div></div>";
    }
    $pdo = null;
?>