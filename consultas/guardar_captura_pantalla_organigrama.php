<?php 
    include "../conexion.php";


    $img = $_POST['captura_img'];
    $nombre = $_POST['nombre'];
    $correo = $_POST['correo'];
    $folio = $_POST['folio'];
    $resultado = [];

    $fileData = base64_decode($img);
    $filename = $nombre.'-'.$correo;
    //path where you want to upload image
    $file = $_SERVER['DOCUMENT_ROOT'] . '/f360/img/organigramas/'.$filename.'.png'; //en donde vas a guardar la imagen
    $imageurl  = 'img/organigramas/'.$filename.'.png'; //url final de la imagen 
    file_put_contents($file,$fileData); //guardas la image....... en donde / el archivo 
    $imageurl;

    array_push($resultado, $imageurl, $file);

    if (file_exists($file)) {
        $resp = "existe"; 

        $sql = "UPDATE solicitud_de_servicios SET CheckOrg = 2, Estatus = 'ASIGNAR ENCUESTA' WHERE IDsolicitud = ?";
        $stmt = $conn -> prepare ($sql);
        $stmt -> execute([$folio]);      
        
        array_push($resultado, $resp);

    } else {
        $resp = "no existe"; 
        array_push($resultado, $resp);
    }

    echo json_encode($resultado);

?>