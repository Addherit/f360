<?php 
    include "../conexion.php";

    $id_a_eliminar = $_POST['id_seccion'];

    $sql = "DELETE FROM secciones WHERE Id_seccion = ?";
    $stmt = $conn -> prepare($sql);
    $stmt -> execute([$id_a_eliminar]); 
    $validador = $stmt -> rowCount();

    if($validador){
        echo "<div class='row' style='background-color: #bddcbd; color: green; padding-top: 10px; padding-bottom: 10px; border-radius: 5px; margin: 0px; margin-bottom: 5px'><div class='col-sm-12'><span>Sección eliminada correctamente</span></div></div>";
    } else {
        echo "<div class='row' style='background-color: #f9a8a8; color: #ad0b0b; padding-top: 10px; padding-bottom: 10px; border-radius: 5px; margin: 0px; margin-bottom: 5px'><div class='col-sm-12'><span><b>ERROR</b> al tratar de eliminar sección, favor de ponerse en contacto con sistemas</span></div></div>";
    }
?>