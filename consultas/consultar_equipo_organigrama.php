<?php 
    include "../conexion.php";

    $id_servicio_solicitado = $_POST['id_de_servicio'];

    if (isset($_POST['nombre_participante'])) {
        $nombre_participante = $_POST['nombre_participante'];
        $sql = "SELECT * FROM equipo_organigrama WHERE Id_folio_servicio_solicitado = '$id_servicio_solicitado' AND Nombre = '$nombre_participante' ";
    } else {
        $sql = "SELECT * FROM equipo_organigrama WHERE Id_folio_servicio_solicitado = '$id_servicio_solicitado'";
    }
       
    $stmt = $conn -> query($sql);
    $respuesta_array = [];

    if ($stmt) {
        
        foreach ($stmt as $row) {
            $linea = [
                "id_equipo_organigrama" => $row["Id_equipo_organigrama"],
                "id_folio_servicio_solicitado" => $row["Id_folio_servicio_solicitado"],
                "nombre" => $row["Nombre"],
                "email" => $row["Email"],
                "ya_contesto" => $row["Ya_contesto"],
                "es_lider" => $row["Es_lider"],
            ];  
            array_push($respuesta_array, $linea);
        }
        echo json_encode($respuesta_array); 
    }

    $stmt->closeCursor(); // opcional en MySQL, dependiendo del controlador de base de datos puede ser obligatorio
    $stmt = null; // obligado para cerrar la conexión
    $pdo = null;

?>