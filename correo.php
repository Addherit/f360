<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>    
    <div class="container">       
        <br>
        <div class="card mb-4" style="border-top: 5px solid #e31f1f;">
            <div class="card-body">      
            <h3 class="text-center"><b>Gracias por tú participación</b></h3>  
            <div class="col-12 text-center">
            <img class="mx-auto mt-4 mb-3" width="200" src="https://www.f360.com.mx/wp-content/uploads/2019/02/7cbc92_b6789845a085421cb08c1e8b526fc926_mv2.png" />
            </div> 
                    
            <hr/>

            <h5 class="text-center"><strong>Instrucciones</strong></h5>
            <p>Le invitamos a responder el siguiente formulario y el organigrama con respecto a su equipo a cargo</p>            
            <strong>El link del formulario es el siguiente: </strong> <span><a href="#">Formulario de ingreso</a></span>
            </div>
        </div>

        <div class="text-center text-muted">
            <p>© F360° 2019</p>    
            <span>Av. Mariano Otero No. 1105, Primer piso</span><br>
            <span>Col. Rinconadas del bosque</span><br>
            <span>Guadalajara, Jalisco</span>
        </div>

        </div>
    </div>    
    <script src="js/perfil_usuario.js"></script>
</body>
</html>