<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>F360</title>
</head>
<body onload="consultar_competencia()">
    <?php include "header.php" ?>
    <div class="container-fluid">
        <div class="row">
            <?php include "sidebar.php"?>   
            <?php include "modales.php"?>          
            <main role="main" class="col-md-9 col-lg-9 col-xl-10 ml-sm-auto">
                <div class="d-flex justify-content-between align-items-center pt-3 mb-3 border-bottom">
                    <h1 class="h2 col-md-6">Comentarios Finales</h1>             
                    <div class="col-md-6 text-right">                        
                    </div>        
                </div>               
                <div class="col-md-12">                                     
                    
                </div>                                
            </main>
        </div>
    </div>
    <?php include "footer.php" ?>
    <script src="js/comentarios_finales.js"></script>
</body>
</html>