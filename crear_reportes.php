<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>F360</title>
</head>
<body onload="consultar_solicitud_de_servicios()">
    <?php include "header.php" ?>
    <div class="container-fluid">
        <div class="row">
            <?php include "sidebar.php"?>   
            <?php include "modales.php"?>          
            <main role="main" class="col-md-9 col-lg-9 col-xl-10 ml-sm-auto">
                <div class="d-flex justify-content-between align-items-center pt-3 mb-3 border-bottom">
                    <h1 class="h2 col-md-6">Generar reporte final</h1>                     
                </div>        
               
                <div class="col-md-12"> 
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-search"></i></span>
                        </div>
                        <input id="filtroEquipoDeTrabajo" type="text" class="form-control col-md-6 filtroBusqueda" placeholder="Buscar por ID, Nombre, correo electrónico o estatus" onkeyup="consultar_solicitud_de_servicios()">
                        <div class="spin" style="margin-left: 5px; display: none"><span class="spinner"></span></div>
                        <div class="row mensaje"></div>
                        
                    </div>
                    <div class=" table-responsive">
                        <table class="table table-striped table-sm table-bordered table-hover text-center" id="tbl_solicitud_servicios" style="white-space: nowrap">
                            <thead style="background-color: #16195c; color: white">
                                <tr>
                                    <th colspan=2></th>                                    
                                    <th scope="col">ID</th>
                                    <th scope="col">Nombre del solicitante</th>
                                    <th scope="col">Correo electrónico</th>
                                    <th scope="col">Tipo de servicio</th>
                                    <th scope="col">Fecha de solicitud</th>
                                    <th scope="col">Estatus</th>        
                                    <th></th>                            
                                </tr>
                            </thead>
                            <tbody>                                
                            </tbody>
                        </table>
                    </div>
                    
                </div>
                
            </main>
        </div>
    </div>
    <?php include "footer.php" ?>
    <script src="js/crear_reportes.js"></script>
</body>
</html>