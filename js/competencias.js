function crear_nueva_competencia() { //listo
    
    $(document).on('submit',function(event){
        event.preventDefault();
        
        const form = document.getElementById('form_crear_competencia');
        const formData = new FormData(form);

        $.ajax({
            url: 'consultas/insertar_competencias.php',
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,
        }).done(function (response) {   
            $(".mensaje_modal_competencias").empty();               
            $(".mensaje_modal_competencias").append(response);                 
            setTimeout(function () { 
                $('#modal_crear_competencias').modal('hide')
            } , 2200);  
        })

    })

}

function elimnar_competencia(id_a_eliminar) { //listo   
    var id_a_eliminar = id_a_eliminar.id.substring(13)
    resultado = confirm("esta seguro de eliminar el registro "+ id_a_eliminar+" ?")    
    if (resultado) {
        $.ajax({
            url:'consultas/eliminar_competencia.php',
            type:'POST',
            data:{id_competencia: id_a_eliminar}
        }).done( function (response) {
                $(".mensaje").empty()
                $(".mensaje").append(response)
                setTimeout('location.reload()' , 2500);          
        })        
    }    
}

function consultar_competencia() { //listo
    
    var valorEscrito = $("#filtro_competencias").val()   
    $.ajax({        
        url:'consultas/consultar_competencias.php',
        type: 'POST',
        data: {valorEscrito: valorEscrito}, 
        dataType:'json',       
    }).done( function(response){     
        var resp = "";              
        $("#tbl_competencias tbody").empty();   
        
            if(response[0] != 'false'){
                
                for(x = 0; response.length > x; x++ ) {
                    resp = '<tr id="fila_solicitud_'+response[x][0]+'" style="cursor:pointer" title="Doble click para asignar encuesta" ondblclick="asignar_encuesta(this)">'
                    resp +='<td> <a href="#" style="color: red" id="btn_eliminar_'+response[x][0]+'" onclick="elimnar_competencia(this)"><i class="fas fa-trash" style="color: red"></i> Eliminar<a></td>'
                    resp +='<td> <a href="#" style="color: #c2c229" id="editar_competencia_'+response[x][0]+'" onclick="mostrar_datos_para_editar(this)"><i class="fas fa-pencil-alt" style="color: #c2c229"></i> Editar</a></td>'
                    resp +='<td>'+response[x][0]+'</td>'     
                    resp +='<td>'+response[x][1]+'</td>'  
                    resp +='<td>'+response[x][2]+'</td>'                    
                    resp +='<td>'+response[x][3]+'</td>'                                           
                    resp +='</tr>';
                    $("#tbl_competencias tbody").append(resp);
                  }

            } else {
                 resp = '<tr><td colspan="7">No hay registro que coincida</td></tr>'
                 $("#tbl_competencias tbody").append(resp);
            }
        
        $(".spinner").css('display', 'none');    
    });
    
}

function mostrar_datos_para_editar(id_a_editar) { //listo    

    var id_a_editar = id_a_editar.id.substring(19)      
      
    $.ajax({        
        url:'consultas/consultar_competencias.php',
        type: 'POST',
        data: {id_competencia: id_a_editar}, 
        dataType:'json',       
    }).done( function(response){     
        console.log(response);
        
        $("#modal_editar_competencias").modal('show')
        
        $(".id_competencia_a_editar").empty()
        $(".id_competencia_a_editar").append(response[0][0])       
        
        $("#id_competencia_a_editar_modal_editar").val(response[0][0])  

        $("#estatus_competencia_modal_editar").val(response[0][3])

        $("#nombre_competencia_modal_editar").val(response[0][2])      
        $("#editar_naturaleza_competencia").val(response[0][1]) 

        

        
    });       
}

function editar_competencia() {  //listo      
    
     $(document).on('submit',function(event){
        event.preventDefault();
        
        const form = document.getElementById('form_editar_competencia');
        const formData = new FormData(form);

        $.ajax({
            url: 'consultas/editar_competencias.php',
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,
        }).done(function (response) {
            $(".mensaje_modal_editar_competencia").empty(); 
            $(".mensaje_modal_editar_competencia").append(response);                   
            setTimeout('location.reload()', 2500);  
        })

    })
}


