//Generate PDF
var id_solicitud = $("#id_solicitud").val();

llamar_img_organigrama(id_solicitud)
crear_tabla_competencias(id_solicitud)

function contar_numero_paginas() {
    var numero_de_pagina 
    $(".print-wrap").each(function( index ) {                        
        numero_de_pagina = index; //index empieza en cero
    });        
    return numero_de_pagina
}

function completar_indice_con_naturalezas(naturalezas) {
    var tag_li = ""
    naturalezas.forEach(naturaleza_actual => {
        tag_li = '<li><h3><b>'+naturaleza_actual+'</b><b style="float: right">4</b></h3></li>'
        $("#li-naturalezas").append(tag_li)
    });
    
}



function llamar_img_organigrama(id_solicitud) {        
    $.ajax({
        url: 'consultas/mostrar_organigrama_img.php',
        type: 'POST',
        data: {id: id_solicitud},
        dataType: 'JSON',
    }).done( function (response) {    
        console.log(response);
          
        $("#nombre_perfil_participante").append(response[0])
        $("#puesto_perfil_participante").append(response[2])
        $("#correo_perfil_participante").append(response[1])
        var url = "img/organigramas/"+response[0]+"-"+response[1]+".png";
        $("#can_img_organigrama").html('<img src="'+ url +'" alt="imagen" style="width: 100%;">');
    })
}

function crear_nueva_pagina_preguntas_x2 (name_page) {
    var name_page = "preguntas_con_respuesta_de_"+name_page
    
    var numero_de_pagina = parseInt(contar_numero_paginas());
    numero_de_pagina =  numero_de_pagina + 2
    var page = "page"+numero_de_pagina
    var pagina_nueva = '<div class="row print-wrap '+page+'"><div class="col-12"><div id="'+name_page+'"></div></div></div>'    
    $("#pages-container").append(pagina_nueva)

    return name_page
}

function crear_tabla_competencias (id_solicitud){
    $.ajax({
        url: 'consultas/crear_tabla_competencias.php',
        type: 'POST',
        data: {id: id_solicitud},
        dataType: 'JSON',
    }).done( function (response) {        
          
        completar_indice_con_naturalezas(response[0])           
        pintar_tabla_competencias(response)  
        imprimir_poligonos_competencuas_preguntas(response[1])
    })
}

function pintar_tabla_competencias (response) {

    for (x=0; response[0].length > x; x++) {
        
        var naturaleza_actual = response[0][x];
        var encabezado = '<th>'+naturaleza_actual+'</th>';

        $("#tbl_competencias thead tr").append(encabezado);

        for (y=0; response[1][x].length > y; y++) {
            
            var competencia = response[1][x][y][0];                        
            if ($("#fila_"+y).length) {
                // append
                var fila = '<td>'+competencia+'</td>';
                $("#fila_"+y).append(fila);                
            } else {
                //create
                if (x > 0) {
                    var fila = '<tr id="fila_'+y+'">'
                    for (w=0; x > w; w++) {                        
                        fila += '<td></td>';   
                    }
                    fila += '<td>'+competencia+'</td></tr>';
                    $("#tbl_competencias tbody").append(fila);  
                } else {
                    var fila = '<tr id="fila_'+y+'"><td>'+competencia+'</td></tr>';
                    $("#tbl_competencias tbody").append(fila);   
                }   
            }                                                                      
        }
    }    
}

function crear_nueva_pagina_poligono () {

    var numero_de_pagina = parseInt(contar_numero_paginas());
    numero_de_pagina =  numero_de_pagina + 2
    var page = "page"+numero_de_pagina
    var pagina_nueva = '<div class="row print-wrap '+page+'"><div class="col-12"><div id="competencias-desglosadas-'+page+'"></div></div></div>'    
    $("#pages-container").append(pagina_nueva)

    return page
}

function imprimir_poligonos_competencuas_preguntas (competencias) {                   
                       
    competencias.forEach(naturalezas => {            
        var naturaleza_actual = naturalezas[0][1]; //naturaleza - competencia array - nombre competencia/ naturaleza                
        var id_canvas_poligono_actual = 'poligono_'+naturaleza_actual.replace(/ /g, "_");
        var name_page = crear_nueva_pagina_poligono()

        var competencia = '<div class=competencia_'+(naturaleza_actual.replace(/ /g, "_")).replace(".","")+'>'
        competencia += "<h2 style='color: #319fce'>"+naturaleza_actual.substring(0,2)+" Competencia "+naturaleza_actual.substring(2,20)+"</h2>";
        competencia += '<canvas id="'+id_canvas_poligono_actual+'" width="100"></canvas></div>';            
        $("#competencias-desglosadas-"+name_page).append(competencia)          

        var ctx = document.getElementById(id_canvas_poligono_actual).getContext('2d');                                                 
        var competencias_labels = [];      
        naturalezas.forEach(naturaleza_actual => {                            
            var nombre_competencia_actual = naturaleza_actual[0]
            competencias_labels.push(nombre_competencia_actual)          
        }); 
        array_competencias_por_naturaleza (naturaleza_actual, competencias_labels, ctx)   
        var page_name = crear_nueva_pagina_preguntas_x2 (name_page)        
        consultar_preguntas_respuestas_segun_competencia (naturaleza_actual, competencias_labels, page_name)     
    }); 

}

function array_competencias_por_naturaleza (naturaleza_actual, competencias_por_naturaleza, ctx) {
    var id_solicitud = $("#id_solicitud").val();    

    $.ajax({
        url: 'consultas/consulta_preguntas_respuestas_encuestas.php',
        type: 'POST',
        data: {naturaleza_actual: naturaleza_actual, id_solicitud: id_solicitud },
        dataType: 'JSON'
    }).done((response) => {       
       
        var array_valores_competencias = []
        var array_valores_lider = crear_array_promedio_por_competencia(competencias_por_naturaleza, response[0])
        var array_valores_promedio = crear_array_promedio_por_competencia(competencias_por_naturaleza, response[1])
        array_valores_competencias.push(array_valores_lider, array_valores_promedio)        
        
        var competencias_labels = crear_array_labels_para_poligono(array_valores_competencias[0])
        var valores_labels = crear_array_valores_para_poligono(array_valores_competencias[0])
        var valores_labels_promedio = crear_array_valores_para_poligono(array_valores_competencias[1])
        
        alimentar_poligono_con_valores_labels(competencias_labels, valores_labels, valores_labels_promedio, ctx)      

        var competencia_mas_desarrollada = competencia_mas_desarrollada_funcion(array_valores_competencias)      
        var promedio_autoevaluacion = obtener_promedio_poligonos(valores_labels)        
        var promedio_equipo = obtener_promedio_poligonos(valores_labels_promedio)        

        var div_class_competencia = 'competencia_'+(naturaleza_actual.replace(/ /g, "_")).replace(".","");
        $('.'+div_class_competencia).append("<br><h5 style='font-size: 2rem'>Calificación promedio <span style='color: red'>(Rojo)</span>:"+promedio_equipo+" </h5>");
        $('.'+div_class_competencia).append("<h5 style='font-size: 2rem'>Calificación autoevaliación <span style='color: blue'>(Azul)</span>:"+promedio_autoevaluacion+" </h5><br>");
        var competencia_mas_desarrollada_descripcion = "La competencia más desarrollada es "+competencia_mas_desarrollada+", la cual permite conocer a la empresa y sobre todo proponer planes de mejora a las áreas de oportunidad de la misma. Es importante traajar en Gestión de recursos a fin de optimizar ls recursos técnicos, financieros y humanos disponibles."
        $('.'+div_class_competencia).append("<p style='font-size: 1.5rem'>"+competencia_mas_desarrollada_descripcion+"</p>")                
               
    })    
}

function competencia_mas_desarrollada_funcion (array_valores_competencias) {

    var mayor = 0;
    for(i = 0; array_valores_competencias[0].length > i; i++){
        if (array_valores_competencias[0][i][1] > mayor) {                                        
            mayor = array_valores_competencias[0][i][1];            
            var competencia_mas_desarrollada =  array_valores_competencias[0][i][0];                
        }
    }    
    return competencia_mas_desarrollada
}

function obtener_promedio_poligonos (arrya_resultados) {
        
    var suma = 0
    var promedio = 0 
    var cantidad_de_valores = parseInt(arrya_resultados.length)
    arrya_resultados.forEach(element => {
        suma =  parseInt(element) + suma  
    })
    promedio = suma / cantidad_de_valores
    return promedio
}

function crear_array_promedio_por_competencia (competencias_por_naturaleza, array_pregunta_respuesta) {

    array_retorno = []
    competencias_por_naturaleza.forEach(competencia => {            
        var suma = 0
        var promedio = 0   
        var cantidad = 0
        array_pregunta_respuesta.forEach(element => { //recorremos el array de las respuestas            
            if (element.competencia_asignada === competencia) {
                var respuesta = parseInt(element.respuesta)
                suma = respuesta + suma
                cantidad++
            }            
        })
        promedio = suma / cantidad  
        array_retorno.push( linea = [competencia, promedio]);  
    });
    return array_retorno
}

function  alimentar_poligono_con_valores_labels (competencias_labels, valores_labels, valores_labels_promedio, ctx) {

    var myRadarChart = new Chart(ctx, {
        type: 'radar',
        data: {
            labels: competencias_labels,
            datasets: [
                {
                    label: 'Calificación del evaluado',
                    data: valores_labels,                    
                    backgroundColor: ['rgba(48, 138, 166, 0.2)'],
                    borderColor: ['rgba(48, 138, 166, 1)'],
                }, 
                {
                    label: 'Calificación promedio',
                    data: valores_labels_promedio, 
                    backgroundColor: ['rgba(86, 7, 12, 0.2)'],
                    borderColor: ['rgba(86, 7, 12, 1)'],
                }
            ]
        },
    })
}

function crear_array_labels_para_poligono (array_valores_competencias) {
    array = []
    for (d=0; array_valores_competencias.length > d ;d++) {                        
        array.push(array_valores_competencias[d][0])
    }       
    return array
}

function crear_array_valores_para_poligono (array_valores_competencias) {
    array = []
    for (d=0; array_valores_competencias.length > d ;d++) {            
        array.push(array_valores_competencias[d][1])
    }
    return array
}

function consultar_preguntas_respuestas_segun_competencia (naturaleza_actual, competencias_labels, page_name) {
    
    var id_solicitud = $("#id_solicitud").val();        
    $.ajax({
        url: 'consultas/consulta_preguntas_respuestas_encuestas.php',
        type: 'POST',
        data: {naturaleza_actual: naturaleza_actual, id_solicitud: id_solicitud },
        dataType: 'JSON'
    }).done((array_lider_equipo_respuestas) => {                
                  
        competencias_labels.forEach(competencia_actual => {
                                 
            var descripcion_competencia = obtener_descripcion_de_competencia (array_lider_equipo_respuestas[0], competencia_actual)
            var nombre_id = "grafica_horizontal_"+(competencia_actual.replace(/ /g, "_")).replace(".","")

            $("#"+page_name).append('<h3><u>'+competencia_actual+'</u></h3><p style="font-size: 1.5rem">'+descripcion_competencia+'</p><canvas id="'+nombre_id+'" width="400" height="80"></canvas>') 

            var array_preguntas_por_competencia = []
            var array_respuestas_por_competencia = []            
            array_lider_equipo_respuestas[0].forEach(pregunta_respuesta => {                
                
                if (pregunta_respuesta.competencia_asignada === competencia_actual) {
                    var pregunta = pregunta_respuesta.pregunta
                    var respuesta = pregunta_respuesta.respuesta                              
                    array_preguntas_por_competencia.push(pregunta)
                    array_respuestas_por_competencia.push(respuesta)                   
                }
            });                                    
            
            pintar_grafica_horizontal_preguntas(array_preguntas_por_competencia, array_respuestas_por_competencia, nombre_id)
        });                                
    })
}

function pintar_grafica_horizontal_preguntas (array_preguntas_por_competencia, array_respuestas_por_competencia, nombre_id) {

    var ctx = document.getElementById(nombre_id).getContext('2d');
    Chart.defaults.scale.ticks.beginAtZero = true;
    var myBarChart = new Chart(ctx, {
        type: 'horizontalBar',
        data: {
            labels: array_preguntas_por_competencia,
            datasets: [
                {
                    label: 'Calificación del evaluado',
                    data: array_respuestas_por_competencia, 
                    backgroundColor: ['rgba(48, 138, 166, 0.2)', 'rgba(54, 162, 235, 0.2)', 'rgba(54, 162, 235, 0.2)',],
                    borderColor: ['rgba(48, 138, 166, 1)', 'rgba(48, 138, 166, 1)', 'rgba(48, 138, 166, 1)'],
                },                
            ]
        },    
    });
   
}



function obtener_descripcion_de_competencia (array_lider_equipo_respuestas, competencia_actual) {

    var descripcion = ""
    array_lider_equipo_respuestas.forEach(pregunta_respuesta => {         
        if (pregunta_respuesta.competencia_asignada === competencia_actual) {
            descripcion = pregunta_respuesta.descripcion_competencia
        }
    }); 
    return descripcion  

}
