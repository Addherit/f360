
//obtener el primer nodo    
$(function() {
  $("#btn-logout").hide()
  $("#btn-delete-nodes").hide()
  var datasource = {
    'name': $("#nombre_usuario").val(),
    'title': $("#correo_electronico").val(),     
  };

  var equipo = new Object({
    nombre: $("#nombre_usuario").val(),
    correo: $("#correo_electronico").val(),  
    children: [],
    parent: [],    
    siblings: [],
  });
  // console.log(equipo);
  


  var getId = function() { 
    return (new Date().getTime()) * 1000 + Math.floor(Math.random() * 1001);
  };

  var oc = $('#chart-container').orgchart({
    'data' : datasource,
    'chartClass': 'edit-state',    
    'nodeContent': 'title',
    'exportButton': true,
    'exportFilename': 'SportsChart',
    'parentNodeSymbol': 'fa-th-large',
    'createNode': function($node, data) {
      
      var  nodo_maestro = getId();
      $node[0].id = nodo_maestro      
      
    }
    
    
  });

  // SE COMENTA LA LINEA PARA SELECCIONAR EL NODO QUE EL USUARIO queueMicrotask, SE DEJA EN AUTOMATICO
  // oc.$chartContainer.on('click', '.node', function() {            
    // var $this = $(this);
    var $this = $(".node");
    console.log($this);    
    
    $('#selected-node').val($this.find('.title').text()).data('node', $this);



  // });

    oc.$chartContainer.on('click', '.orgchart', function(event) {
      if (!$(event.target).closest('.node').length) {
        $('#selected-node').val('');
      }
    });

    $('input[name="chart-state"]').on('click', function() {
      $('.orgchart').toggleClass('edit-state', this.value !== 'view');
      $('#edit-panel').toggleClass('edit-state', this.value === 'view');
      if ($(this).val() === 'edit') {
        $('.orgchart').find('tr').removeClass('hidden')
          .find('td').removeClass('hidden')
          .find('.node').removeClass('slide-up slide-down slide-right slide-left');
      } else {
        $('#btn-reset').trigger('click');
      }
    });

    $('input[name="node-type"]').on('click', function() {
      var $this = $(this);
      if ($this.val() === 'parent') {
        $('#edit-panel').addClass('edit-parent-node');
        $('#new-nodelist').children(':gt(0)').remove();
      } else {
        $('#edit-panel').removeClass('edit-parent-node');
      }
    });    

    $('#btn-add-nodes').on('click', function() {            
      $("#btn-delete-nodes").show()
      var $chartContainer = $('#chart-container');            
      var nodeVals = [];
      $('#new-nodelist').find('.new-node').each(function(index, item) {
        var validVal = item.value.trim();
        if (validVal.length) {
          nodeVals.push(validVal);

          var tipo = $('input[name="node-type"]:checked').val()
          var nombre = $(".new-node").val()
          var correo = $(".new-node-email").val()
          var nuevo = {nombre: nombre, email: correo }

          switch (tipo) {
            case 'children':
              equipo.children.push(nuevo)
              break;
            case 'siblings':  
            // if (equipo.parent.length) {
            //   equipo.siblings.push(nuevo)              
            // }              
              break;
            case 'parent':
              equipo.parent.push(nuevo)
              break;
          }                                                 
          console.log(equipo);
          console.log(tipo);                              
        }
      });      
      
      var correo = $(".new-node-email").val();

      var $node = $('#selected-node').data('node');
      console.log($node);
      
      if (!nodeVals.length) {
        alert('Favor de completar los campos de nuevo locaborador y correo electronico');
        return;
      }
      var nodeType = $('input[name="node-type"]:checked');
      if (!nodeType.length) {
        alert('Selecciona una gerarquia');
        return;
      }
      if (nodeType.val() !== 'parent' && !$('.orgchart').length) {
        alert('Favor de construir un nodo ROOT si quieres construir una organización');
        return;
      }
      if (nodeType.val() !== 'parent' && !$node) {
        alert('Selecciona un nodo de la organización');
        return;
      }
      if (nodeType.val() === 'parent') {
        if (!$chartContainer.children('.orgchart').length) {// if the original chart has been deleted
          oc = $chartContainer.orgchart({
            'data' : { 'name': nodeVals[0], 'title': correo },
            'nodeContent': 'title',
            'exportButton': true,
            'exportFilename': 'SportsChart',
            'parentNodeSymbol': 'fa-th-large',
            'createNode': function($node, data) {
              $node[0].id = getId();               
            }
          });
          oc.$chart.addClass('view-state');
        } else {
          oc.addParent($chartContainer.find('.node:first'), { 'name': nodeVals[0], 'title':correo, 'id': getId() });
          // var nombre = $(".new-node").val()
          // var correo = $(".new-node-email").val()
          // var nuevo = {nombre: nombre, email: correo }
          // equipo.parent.push(nuevo)
          // console.log(equipo);
        }
      } else if (nodeType.val() === 'siblings') {
        if ($node[0].id === oc.$chart.find('.node:first')[0].id) {
          alert('No está permitido agregar iguales al nodo principal ');
          return;
        }
        oc.addSiblings($node, nodeVals.map(function (item) {
            return { 'name': item, 'title': correo, 'relationship': '110', 'id': getId() };
          }));          

          // //por mi
          // var nombre = $(".new-node").val()
          // var correo = $(".new-node-email").val()
          // var nuevo = {nombre: nombre, email: correo }
          // equipo.siblings.push(nuevo)
          // console.log(equipo);
          // //
          
      } else {
        var hasChild = $node.parent().attr('colspan') > 0 ? true : false;
        if (!hasChild) {
          var rel = nodeVals.length > 1 ? '110' : '100';

          //por mi 
          var id_nuevo_nodo = $node[0].attributes.id
          var nombre = $(".new-node").val()
          var correo = $(".new-node-email").val()
          var nuevo = {nombre: nombre, email: correo, id: id_nuevo_nodo }
          //
          
          oc.addChildren($node, nodeVals.map(function (item) {
              return { 'name': item, 'title': correo, 'relationship': rel, 'id': getId() };
            }));

            // //por mi
            // equipo.children.push(nuevo)
            // console.log(equipo);
            // //
          
        } else {
          oc.addSiblings($node.closest('tr').siblings('.nodes').find('.node:first'), nodeVals.map(function (item) {
              return { 'name': item, 'title': correo, 'relationship': '110', 'id': getId() };
            }));
          
            // //por mi
            // equipo.children.push(nuevo)
            // console.log(equipo);
            // //
        }
      }
      console.log(nodeVals);
      
    });

    $('#btn-delete-reload').on('click', function() {
      // var $node = $('#selected-node').data('node');   
      alert()
      var confirmar = window.confirm('Desea borrar todo los cambios?')
      if (confirmar) {
       location.reload();
      }

      //ESTO BORRA POR NODO. PERO TENIA PEDOS AL HACER MIS AMIGUITOS EN SU JSON
      // if (!$node) {
      //   alert('Favor de seleccionar un nodo como referencia');
      //   return;
      // } else if ($node[0] === $('.orgchart').find('.node:first')[0]) {
              
      //   if (!window.confirm('Are you sure you want to delete the whole chart?')) {
      //     return;
      //   }
      // }
      // oc.removeNodes($node);
      // $('#selected-node').val('').data('node', null);                               
      
    });

    $('#btn-reset').on('click', function() {
      $('.orgchart').find('.focused').removeClass('focused');
      $('#selected-node').val('');
      $(".new-node-email").val('');
      $(".new-node").val('');      
      $('#node-type-panel').find('input').prop('checked', false);
    });

  });


  

  
