function consultar_solicitud_de_servicios () { //listo
    const valor = new FormData()     
    valor.append('valorEscrito', document.querySelector("#filtroEquipoDeTrabajo").value)
    
    fetch('consultas/consultar_solicitud_de_servicios.php',{ method: 'POST', body: valor    })
    .then(response => response.json())
    .then(response => {
        $("#tbl_solicitud_servicios tbody").empty();            
        
        if(response[0] != 'false') {                                        
            for(x = 0; response.length > x; x++ ) {
            
                if (response[x][4] == 'LISTO PARA REPORTEO') {
                    var btn_reporteo_pdf = '<td><a id="btn_crear_pdf_link" href="vista_pdf_creado.php?folio_servicio='+response[x][0]+'&participante='+response[x][1]+'" title="Crear Reporte en PDF" style="color: red"><i class="fas fa-file-pdf"></i> Crear PFD</a></td>'
                } else {
                    var btn_reporteo_pdf = '<td><span title="Aun no se puede generar reporte" style="color: grey"><i class="fas fa-file-pdf"></i> Crear PFD</a></td>'
                }
                resp = '<tr id="fila_solicitud_'+response[x][0]+'" style="cursor:pointer" title="Doble click para asignar encuesta" ondblclick="asignar_encuesta(this)">'
                resp += '<td> <a href="#" style="color: red" id="btn_eliminar_'+response[x][0]+'" onclick="eliminar_solicitud_servicio(this)"><i class="fas fa-trash" style="color: red"></i> Eliminar<a></td>'
                resp += '<td> <a href="#" style="color: #c2c229" id="btn_editar_'+response[x][0]+'" onclick="mostrar_solicitud_a_editar_modal(this)"><i class="fas fa-pencil-alt" style="color: #c2c229"></i> Editar</a></td>'
                resp += '<td>'+response[x][0]+'</td>'
                resp += '<td>'+response[x][1]+'</td>'
                resp += '<td>'+response[x][5]+'</td>'
                resp += '<td>'+response[x][2]+'</td>'
                resp += '<td>'+response[x][3]+'</td>'
                resp += '<td>'+response[x][4]+'</td>'
                resp += btn_reporteo_pdf
                resp += '</tr>';
                $("#tbl_solicitud_servicios tbody").append(resp);
                }

        } else {
            resp = '<tr><td colspan="9">No hay registro que coincida</td></tr>'
            $("#tbl_solicitud_servicios tbody").append(resp);
        }       
                    
    })
}

function enviar_correo_segun_servicio_solicitado () {
    var seleccionado = ''    
    var tipo_servicio = document.querySelectorAll('.form-check-input')
    tipo_servicio.forEach(element => {            
        if (element.checked) { seleccionado = element.value }
    })
    if (seleccionado) {
        var data = new FormData()
        data.append('email', document.querySelector("#email_escrito").value)
        data.append('tipo_servicio', seleccionado)
        fetch('consultas/insert_solicitud_y_enviar_correo.php', { s
            method: 'POST',
            body: data        
        })
        .then(data => data.text())
        .then(data => {
            $(".mensaje").append(data)        
            $("#modal_mandar_formulario").modal('hide');                      
            // setTimeout('location.reload()' , 2500);
        })
        // $.ajax({
        //     url: 'consultas/insert_solicitud_y_enviar_correo.php',
        //     type: 'POST',
        //     data: {
        //         email: email,
        //         tipo_servicio: tipo_servicio
        //     },
        // }).done ((response) => {  
        //     $(".mensaje").append(response)        
        //     $("#modal_mandar_formulario").modal('hide');                      
        //     setTimeout('location.reload()' , 2500);
        // })
    } else {        
        $(".mensaje-modal-enviar-correo").append("<div class='row' style='background-color: #f9a8a8; color: #ad0b0b; padding-top: 10px; padding-bottom: 10px; border-radius: 5px; margin: 0px; margin-bottom: 5px'><div class='col-sm-12'><span><b>ERROR</b> Favor de serleccionar un TIPO DE SERVICIO</span></div></div>")
    }   
}

function eliminar_solicitud_servicio(id_a_eliminar) {    //listo
    var id_a_eliminar = id_a_eliminar.id.substring(13)
    resultado = confirm("esta seguro de eliminar el registro "+ id_a_eliminar+" ?")    
    
    if (resultado) {
       $.ajax({
           url:'consultas/eliminar_solicitud_de_servicios.php',
           type:'post',
           data:{id_a_eliminar: id_a_eliminar}
       }).done((response) => {
           $(".mensaje").empty()
           $(".mensaje").append(response)
           setTimeout('location.reload()' , 2500);  
       })
    }    
}

function mostrar_solicitud_a_editar_modal(id_a_editar) {

    var id_a_editar = id_a_editar.id.substring(11) 
    
    rellenar_enucestas_select_solicitud_servicios()
    pintar_donuts_graficas(id_a_editar)
    acomodar_info_en_inputs(id_a_editar)
    mostrar_equipo_de_solicitud(id_a_editar)
    agregar_href_btn_vista_previa(id_a_editar)

    $("#modal_notificaciones_de_servicio").modal('show')

}

function rellenar_enucestas_select_solicitud_servicios() { //listo

    $.ajax({
        url:'consultas/consulta_catalogo_encuestas.php',
        type:'POST',
        data:{id_encuesta: ""},
        dataType:'json'
    }).done ((response) => {        
        console.log(response);
        
        $("#slt_encuestas_disponibles_modal_editar").empty()
        var option_encuestas_solicitud_servicios = '<option value="false" disabled selected>Asigna una encuesta</option>'
            $("#slt_encuestas_disponibles_modal_editar").append(option_encuestas_solicitud_servicios)
        for (x = 0;  x < response.length; x++) {            
            var option_encuestas_solicitud_servicios = '<option value="'+response[x].id_encuesta+'">'+response[x].titulo_encuesta+'</option>'
            $("#slt_encuestas_disponibles_modal_editar").append(option_encuestas_solicitud_servicios)
        }                
    })  
}

function acomodar_info_en_inputs(id_a_editar) {

    document.getElementById("slt_encuestas_disponibles_modal_editar").disabled = true;
    var data = new FormData()
    data.append('valorEscrito', id_a_editar)

    fetch('consultas/consultar_solicitud_de_Servicios.php', { method: 'POST', body: data })
    .then(response => response.json())
    .then(response => {
        // cabecera
        $("#id_notificaciones_de_Servicio_editar").empty()
        $("#id_notificaciones_de_Servicio_editar").append(response[0][0])

        //body    
        $("#id_solicitud_servicio_modal_hide").val(response[0][0])
        $("#persona_solicitante_del_servicio").val(response[0][1])
        $("#correo_persona_solicitante_modal_editar").val(response[0][5])  
        $("#fecha_solicitud_modal_editar").val(response[0][3])
        $("#estatus_notificacion_servicio_editar").val(response[0][4]) 
        $("#slt_encuestas_disponibles_modal_editar").val(response[0][7])
        console.log(response[0][7]);

        $('#btn_crear_pdf_link').each(() => {
            this.href += '?participante='+response[0][1]+'&folio_servicio='+response[0][0];
        })

        if(response[0][4] == 'ASIGNAR ENCUESTA') {        
            document.getElementById("slt_encuestas_disponibles_modal_editar").disabled = false;
        }
    })
}

function editar_solicitud_de_servicios() {
    
        $(document).on('submit', (event) => {
            event.preventDefault();
            if ($("#slt_encuestas_disponibles_modal_editar").val() && $("#estatus_notificacion_servicio_editar").val() == "ASIGNAR ENCUESTA") {
                const form = document.getElementById('form_editar_solicitud_de_servicio');
                const formData = new FormData(form);
        
                $.ajax({
                    url: 'consultas/editar_solicitud_de_servicio.php',
                    type: 'POST',
                    data: formData,
                    processData: false,
                    contentType: false,
                }).done((response) => {

                    $(".mensaje").append(response);    
                    $("#modal_notificaciones_de_servicio").modal('hide')                
                    var id_solicitud_servicio = $("#id_notificaciones_de_Servicio_editar").text()
                    enviar_correo_encuesta_participantes(id_solicitud_servicio)
                
                })
            } else if (!$("#slt_encuestas_disponibles_modal_editar").val() && $("#estatus_notificacion_servicio_editar").val() == "ASIGNAR ENCUESTA") {
                
                $(".mensaje_modal").empty();   
                var respuesta = "<div class='row' style='background-color: #f9a8a8; color: #ad0b0b; padding-top: 10px; padding-bottom: 10px; border-radius: 5px; margin: 0px; margin-bottom: 5px'><div class='col-sm-12'><span><b>ERROR</b> Antes de continuar elige una  encuesta</span></div></div>";
                $(".mensaje_modal").append(respuesta);  
            }
            
        }) 


        // $(document).on('submit',function(event){
        //     event.preventDefault();
        //     $(".mensaje_modal").empty();   
        //     var respuesta = "<div class='row' style='background-color: #f9a8a8; color: #ad0b0b; padding-top: 10px; padding-bottom: 10px; border-radius: 5px; margin: 0px; margin-bottom: 5px'><div class='col-sm-12'><span><b>ERROR</b> Antes de continuar elige una  encuesta</span></div></div>";
        //     $(".mensaje_modal").append(respuesta);   
        // })    
}

function pintar_donuts_graficas(id_a_editar) {


    $.ajax({
        url:'consultas/consultar_equipo_organigrama.php',
        type:'POST',
        data:{id_de_servicio: id_a_editar},
        dataType: 'JSON'
    }).done ((response) => {        
                 
        var encuestas_completas = 0;
        for (x=0; response.length > x; x++) {          
            if (response[x].ya_contesto > 0) {
                encuestas_completas = encuestas_completas + 1
            }
        }

        var encuestas_restantes = response.length - encuestas_completas        
        var ctx = $('#encuestas_contestadas'); 
        var myRadarChart = new Chart(ctx, {
            type: 'doughnut',
            data: {            
                labels: ['Encuestas Contestadas', 'restantes'],
                datasets: [{
                    data: [encuestas_completas, encuestas_restantes],
                    backgroundColor: [
                        'rgba(178,34,34, 1)'
                    ],                  
                }]
            },       
        })        
    })      
}

function mostrar_equipo_de_solicitud(id_de_servicio) {    
    
    $.ajax({
        url:'consultas/consultar_equipo_organigrama.php',
        type: 'POST',
        data: { id_de_servicio: id_de_servicio },
        dataType: 'JSON',
    }).done((response) => {        

        $("#tbl_equipo_solicitante_modal_editar tbody").empty()        

        for (x =0; response.length > x; x++) {

            if (response[x].es_lider > 0 ) {
                var td_es_el = '<td title="Este usuario es el que solicitó el servicio" style="color: #b22222"><i class="fas fa-user"></i></td>'
            } else {
                var td_es_el = '<td></td>'
            }

            //ya contesto
            if (response[x].ya_contesto > 0) {
                var ya_contesto = '<td title="Encuesta comppleta" class="text-center" style="color: green"><i class="fas fa-check-circle"></i></td>'
            } else {
                var ya_contesto = '<td></td>'
            }

            var linea = '<tr>'
            linea += td_es_el
            linea += '<td>'+response[x].nombre+'</td>'
            linea += '<td>'+response[x].email+'</td>'
            linea += ya_contesto // Un 0 es considerado Falso y cualquier otro valor es considerado TRUE.
            linea += '</tr>';
            $("#tbl_equipo_solicitante_modal_editar tbody").append(linea)
        }
        
        
    })
}

function enviar_correo_encuesta_participantes(id_solicitud_servicio) {
    $.ajax({
        url:'consultas/enviar_correo_participantes.php',
        type:'post',
        data: {id_solicitud_servicio:id_solicitud_servicio}
    }).done ((response) => {
        alert(response)       
        setTimeout('location.reload()' , 2500);                
    })
    
}

function agregar_href_btn_vista_previa(id_de_servicio) {
    document.getElementById('btn-vista-previa').href = "vista_previa_respuestas.php?id_solicitud="+id_de_servicio;
}

