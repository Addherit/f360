
var input = document.getElementById("cuentaCorreo");
input.addEventListener("keyup", function(event) {
    event.preventDefault();
    if (event.keyCode === 13) {
        validarCorreoYpassword();
    }
});
var input = document.getElementById("cuentaPassword");
input.addEventListener("keyup", function(event) {
    event.preventDefault();
    if (event.keyCode === 13) {
        validarCorreoYpassword();        
    }
})



function validarCorreoYpassword() { 

    var correo = document.getElementById("cuentaCorreo").value;
    var password = document.getElementById("cuentaPassword").value;

    $.ajax({
        url:'consultas/validarCorreoYpassword.php',
        type:'POST',
        dataType: 'json',
        data:{email:correo, pass:password }
    }).done(function (response) {        
        
        $(".mensaje").empty();        
        
        if(response == '0'){
            var mensaje = '<span class="col-sm-10 offset-sm-1" style=" padding: 5px 0px; color: #c50000; background-color: #ff000036; border-radius: 5px;">Credenciales invalidas, favor de intentar de nuevo</span>'
            $(".mensaje").append(mensaje);
        } else {
            var mensaje = '<span class="col-sm-10 offset-sm-1" style=" padding: 5px 0px; color: green; background-color: #0080003b; border-radius: 5px;">Inicio de sesion correcta</span>';
            $(".mensaje").append(mensaje);
            $(".spin").css("display", "block");
            redireccionSegunDpto(response);            
        }
          
    })
}

function redireccionSegunDpto(response) {        
    setTimeout(window.location.href = 'solicitud_de_servicios.php', 1500);
}




