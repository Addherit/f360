function consultar_preguntas_respuestas() {
    var id_solicitud = $("#id_solicitud").val()
    $.ajax({
        url:'consultas/consulta_preguntas_respuestas_encuestas.php',
        type: 'post',
        data: {id_solicitud: id_solicitud},
        dataType: 'json'
    }).done((response) => { 
        var competencia = "";
        var array_competencias = [];
        var array_promedios = []
        response[0].forEach(element => {            
            if (element.competencia_asignada != competencia) {
                competencia = element.competencia_asignada                           
                array_competencias.push({competencia:competencia})
                var suma = 0
                var cant = 0                

                response[0].forEach(element => {
                    if (element.competencia_asignada === competencia) {
                        suma = suma + parseInt(element.respuesta)
                        cant++                        
                    }
                });                
                
                var promedio = (suma / cant).toFixed(2)

                array_promedios.push({promedio: promedio, competencia: competencia})
                var encabezado = '<tr>'
                encabezado += '<th colspan="2" style="background-color: #383c94; color: white">'+competencia+'</th>'
                encabezado += '<th colspan="2" style="background-color: #383c94; color: white">Promedio: '+promedio+'</th>'
                encabezado += '</tr>'
                $("#tbl-preguntas-respuestas-container tbody").append(encabezado)    
            }
            var detalle = '<tr><td class="text-center">'+element.id_pregunta+'</td><td>'+ element.pregunta+'</td><td class="text-center">'+element.respuesta+'</td><td>'+element.id_participante+'</td></tr>'
            $("#tbl-preguntas-respuestas-container tbody").append(detalle)    
            
        }); 
        rellenar_tabla_fortalezas (array_competencias)
        rellenar_tabla_autoevaluacion(array_promedios)           
    });
}

function rellenar_tabla_autoevaluacion(promedios) {
    let top3 = promedios.sort((a, b) => { return b - a; }).slice(0, 3);    
    let less3 = promedios.slice(promedios.length-3)    

    top3.forEach((element, index) => {        
        var filas = '<tr><td>'+element.competencia+'</td><td>'+less3[index].competencia+'</td></tr>'
        $("#table-autoevaluacion tbody").append(filas)
    });        
}

function rellenar_tabla_fortalezas (competencias) {
    competencias.forEach(element => {
        var filas = '<tr><td>'+element.competencia+'</td><td contenteditable></td></tr>'
        $("#tbl-fortalezas").append(filas)
    });
}

function btn_guardar_comentarios() {
    let id_solicitud = $("#id_solicitud").val()    
    let tabla1 = [], tabla2 = []
    $("#tbl-fortalezas tbody tr").each((index, element) => {
        let linea_single = {"competencia": element.childNodes[0].textContent, "observaciones": element.childNodes[1].textContent}        
        tabla1.push(linea_single)
    })

    $("#tbl-comentarios-finales tbody tr").each((index, element) => {                
        let linea_single = {"competencia": element.children[0].textContent, "observaciones": element.children[1].textContent}       
        tabla2.push(linea_single)
    })

    $.ajax({
        url: 'consultas/insert_comentarios_finales.php',
        type: 'post',
        data: {tabla1: tabla1, tabla2: tabla2, id_solicitud: id_solicitud}
    }).done((response) => {
        console.log(response)
    })
    
    

    
}