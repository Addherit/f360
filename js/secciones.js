function crear_nueva_seccion() { //listo
       
    $(document).on('submit',function(event){
        event.preventDefault();
        
        const form = document.getElementById('form_crear_seccion');
        const formData = new FormData(form);

        $.ajax({
            url: 'consultas/insertar_secciones.php',
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,
        }).done(function (response) {
            $("#modal_crear_seccion").modal('hide')
            $(".mensaje").append(response);                   
            setTimeout('location.reload()' , 2200);  
        })

    })
}

function elimnar_seccion(id_a_eliminar) { //listo

    var id_a_eliminar = id_a_eliminar.id.substring(13)
    resultado = confirm("esta seguro de eliminar el registro "+ id_a_eliminar+" ?")    
    if (resultado) {
        $.ajax({
            url:'consultas/eliminar_seccion.php',
            type:'POST',
            data:{id_seccion: id_a_eliminar}
        }).done( function (response) {
                $(".mensaje").empty()
                $(".mensaje").append(response)
                setTimeout('location.reload()' , 2500);          
        })        
    }  
    
}

function consultar_seccion() { //listo

    var valorEscrito = $("#filtro_secciones").val()   
    $.ajax({        
        url:'consultas/consultar_secciones.php',
        type: 'POST',
        data: {valorEscrito: valorEscrito}, 
        dataType:'json',       
    }).done( function(response){     
        var resp = "";              
        $("#tbl_secciones tbody").empty();   
        
            if(response[0] != 'false'){
                
                for(x = 0; response.length > x; x++ ) {
                    resp = '<tr id="fila_solicitud_'+response[x][0]+'" style="cursor:pointer" title="Doble click para asignar encuesta" ondblclick="asignar_encuesta(this)">'
                    resp +='<td> <a href="#" style="color: red" id="btn_eliminar_'+response[x][0]+'" onclick="elimnar_seccion(this)"><i class="fas fa-trash" style="color: red"></i> Eliminar<a></td>'
                    resp +='<td> <a href="#" style="color: #c2c229" id="editar_seccion_'+response[x][0]+'" onclick="mostrar_datos_para_editar(this)"><i class="fas fa-pencil-alt" style="color: #c2c229"></i> Editar</a></td>'
                    resp +='<td>'+response[x][0]+'</td>'     
                    resp +='<td>'+response[x][1]+'</td>'  
                    resp +='<td>'+response[x][2]+'</td>'                    
                    resp +='<td>'+response[x][3]+'</td>'                                         
                    resp +='</tr>';
                    $("#tbl_secciones tbody").append(resp);
                  }

            } else {
                 resp = '<tr><td colspan="7">No hay registro que coincida</td></tr>'
                 $("#tbl_secciones tbody").append(resp);
            }
        
        $(".spinner").css('display', 'none');    
    });
    
}

function mostrar_datos_para_editar(id_a_editar) { //listo
    
    var id_a_editar = id_a_editar.id.substring(15)
      
    $.ajax({        
        url:'consultas/consultar_secciones.php',
        type: 'POST',
        data: {valorEscrito: id_a_editar}, 
        dataType:'json',       
    }).done( function(response){     
        
        $("#modal_editar_secciones").modal('show')
        $(".id_seccion_a_editar_modal_seccion_editar").empty()  
        $(".id_seccion_a_editar_modal_seccion_editar").append(response[0][0])   
        $(".id_seccion_a_editar_modal_seccion_editar").val(response[0][0])               
        $("#tipo_competencia_modal_seccion_editar").val(response[0][1])
        $("#nombre_seccion_modal_editar").val(response[0][2])        
        $("#estatus_seccion_modal_editar").val(response[0][3])        
    });       
}

function editar_seccion() { //listo

    $(document).on('submit',function(event){
        event.preventDefault();
        
        const form = document.getElementById('form_editar_seccion');
        const formData = new FormData(form);

        $.ajax({
            url: 'consultas/editar_secciones.php',
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,
        }).done(function (response) {
            $("#modal_editar_secciones").modal('hide')
            $(".mensaje").append(response);                   
            setTimeout('location.reload()' , 2500);  
        })

    })
    
}