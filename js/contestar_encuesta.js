function consultar_encuesta() {

    var id_encuesta = document.getElementById("id_encuesta_para_contestar").value
    var id_servicio = document.getElementById("id_servicio_solicitado").value
    var nombre_participante = document.getElementById("persona_asignada_en_link").value

    encontrar_al_lider(id_servicio)
    validar_encuesta_contestada(nombre_participante, id_servicio)
    consultar_encabezado_encuesta(id_encuesta, nombre_participante)  
    consultar_detalle_encuesta(id_encuesta)

}

function consultar_encabezado_encuesta(id_encuesta, nombre_participante) {
    $.ajax({
        url:'consultas/consultar_encabezado_encuesta.php',
        type:'post',
        data: {id_encuesta: id_encuesta},
        dataType: 'json'

    }).done( function (response) {        
                
        $("#titulo_encuesta_que_se_borrara").append('Encuesta '+response[0].titulo_encuesta+' para: '+nombre_participante)

    })
}
 
function consultar_detalle_encuesta(id_encuesta) {
    
    $.ajax({
        url:'consultas/consultar_preguntas_encuesta.php',
        type:'post',
        data: {id_encuesta: id_encuesta},
        dataType: 'json'

    }).done( function (response) {

        var es_lider = document.getElementById('es_lider_input_hidden').value
                      
        response.forEach((response, index) => {   
            index ++
            pintar_pregunta(response, index, es_lider)
        });

    })
    
}

function pintar_pregunta(response, index, es_lider) {
    var participante = document.getElementById('persona_asignada_en_link').value    
    

    if (es_lider > 0 ) {
        var pregunta = response.pregunta                
        var posicionEspacio = pregunta.indexOf(" ") //Obtengo la posición del primer espacio en la cadena                
        var primera_palabra = pregunta.substring(0,posicionEspacio)
        var nueva_pregunta = pregunta.replace(primera_palabra, primera_palabra+"s")        
    } else {
        var pregunta = response.pregunta         
        var posicionEspacio = participante.indexOf(" ") //Obtengo la posición del primer espacio en la cadena        
        
        if (posicionEspacio > 0) {
            var primer_nombre = participante.substring(0,posicionEspacio)        
            var nueva_pregunta = primer_nombre+" "+pregunta 
        } else {               
            var nueva_pregunta = participante+" "+pregunta
        }                               
    }
    
    switch (response.tipo_pregunta) {
        case 'abierta':

            var pregunta = '<div class="form-group">'            
            pregunta += '<input type="hidden" class="form-control" name="num_resp_'+index+'" value="'+response.id_pregunta+'">'
            pregunta += '<label for="respuesta_num_'+response.id_pregunta+'">'+index+'. '+nueva_pregunta+'</label>'
            pregunta += '<input type="text" class="form-control" id="respuesta_num_'+response.id_pregunta+'" name="respuesta_num_'+response.id_pregunta+'">'
            pregunta += '</div><br>'
            
        break;
        case 'multiple':
                
            var pregunta = '<div class="form-group">'            
            pregunta += '<input type="hidden" class="form-control" name="num_resp_'+index+'" value="'+response.id_pregunta+'">'
            pregunta += '<label>'+index+'. '+nueva_pregunta+'</label>'            
            pregunta += '<div id="opc_multiples"></div>'
            pregunta += '</div><br>'

            obtener_opciones_multiples(response.id_pregunta)    

        break;
        case 'ranking':

            var pregunta = '<div class="form-group">'   
            pregunta += '<input type="hidden" class="form-control" name="num_resp_'+index+'" value="'+response.id_pregunta+'">'         
            pregunta += '<label for="num_resp_'+response.id_pregunta+'">'+index+'. '+nueva_pregunta+'</label>'   
            pregunta += '<select id="star_rating_respuesta_'+response.id_pregunta+'" name="respuesta_num_'+response.id_pregunta+'" required>'
            pregunta += '<option value="1">1</option>'
            pregunta += '<option value="2">2</option>'
            pregunta += '<option value="3">3</option>'
            pregunta += '<option value="4">4</option>'
            pregunta += '<option value="5">5</option>'
            pregunta += '</select></div><br><br>'                                                               
        
        break;

    }
    $("#total_respuestas").val(index)
    $("#contenido_preguntas").append(pregunta) 
    
    $(function() {
        $('#star_rating_respuesta_'+response.id_pregunta).barrating({
          theme: 'bars-square',           
          showValues: true,
          showSelectedRating: false
        });
    }); 
}

function obtener_opciones_multiples(num_pregunta) {
    
    $.ajax({
        url:'consultas/consultar_opciones_multiples.php',
        type:'POST',
        data: {id_pregunta: num_pregunta},
        dataType: 'json',
    }).done (function (response) {

        response.forEach((response, index) => {
            index ++;
            var opc_multiples = '<div class="form-check">'
            opc_multiples += '<input type="radio" class="form-check-input" id="opc_multiple_pregunta_'+response.id_pregunta+'" name="respuesta_num_'+response.id_pregunta+'" value="'+response.opcion_multiple+'" required>'
            opc_multiples += '<label class="form-check-label" for="respuesta_num_'+response.id_pregunta+'">'+index+'. '+response.opcion_multiple+'</label>'
            opc_multiples += '</div>'
            $("#opc_multiples").append(opc_multiples)
        })
        

        
        
    })
}

function finalizar_encuesta() {
    
    $(document).on('submit',function(event){
        event.preventDefault();

        const form = document.getElementById('form_encuesta_respuestas');
        const formData = new FormData(form);

        $.ajax({
            url: 'consultas/insertar_respuestas_encuestas.php',
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,            
        }).done(function (response) {
            alert(response)            
            setTimeout(window.location.href = 'https://www.f360.com.mx', 2000);               
        })
    })    
}

function validar_encuesta_contestada(nombre_participante, id_servicio) {

    $.ajax({
        url:'consultas/consultar_equipo_organigrama.php',
        type:'POST',
        data:{
            nombre_participante: nombre_participante, 
            id_de_servicio: id_servicio
        },
        dataType: 'JSON',

    }).done ( function (response) {                   
             
        if (response.length > 0 ) {            
            if (response[0].ya_contesto == '1') {
                alert("Esta encuesta ya se contesto")
                setTimeout(window.location.href = 'https://www.f360.com.mx', 2000);
            }
        }               
    })
}

function encontrar_al_lider(id_servicio) {

    $.ajax({
        url:'consultas/consultar_equipo_organigrama.php',
        type:'POST',
        data:{            
            id_de_servicio: id_servicio
        },
        dataType: 'JSON',

    }).done ((response) => {              

        response.forEach((response) => {               
            if (response.es_lider == "1") {
                $("#es_lider_input_hidden").val(response.nombre)
            }           
        })
    })
    
}

