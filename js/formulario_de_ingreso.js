function validar_folio_inicial () {
    
    var folio = $("#folio").val();    
    saber_tipo_de_Servicio(folio)
    if(folio) {
        consulta_solicitudes_form(folio)        
    } else {        
        $.ajax({ //se asigna un id al nuevo formulario
            url:'consultas/consulta_solicitud_form_num_consecutivo.php',
            type: 'POST',
            data: {folio: folio}, 
            dataType: 'json' 
    
        }).done (function (response) {
            var solicitud_consecutiva = parseInt(response.IDsolicitud)+1;
            console.log(solicitud_consecutiva);
            $("#folio").val(solicitud_consecutiva)                        
        })
    }
    
}

function consulta_solicitudes_form(folio) {

    $.ajax({
        url:'consultas/consulta_solicitud_form.php',
        type: 'POST',
        data: {folio: folio},
        dataType: 'json'

    }).done (function (response) {
        console.log(response);
        mostrar_values_inputs(response)
        
    })

}

function mostrar_values_inputs(res) {
    $("#nombre_1").val(res['nombre_1']); $("#puesto_actual").val(res['puesto_actual']); $("#experiencia_puesto").val(res['experiencia_puesto']); $("#estado_civil").val(res['estado_civil'])
    $("#cumpleanos").val(res['cumpleanos']); $("#aniversario").val(res['aniversario']); $("#telefono_1").val(res['telefono_1']); $("#email").val(res['email'])
    $("#nombre_2").val(res['nombre_2']); $("#giro").val(res['giro']); $("#direccion").val(res['direccion']); $("#telefono_2").val(res['telefono_2'])
    $("#cp").val(res['cp']); $("#email_2").val(res['email_2']); $("#extension").val(res['extension']); $("#nombre_3").val(res['nombre_3']); $("#inicio").val(res['inicio'])
    $("#razones").val(res['razones']); $("#pregunta_1").val(res['pregunta_1']); $("#pregunta_2").val(res['pregunta_2']); $("#pregunta_3").val(res['pregunta_3']); $("#razon_social").val(res['razon_social'])
    $("#domicilio_fiscal").val(res['domicilio_fiscal']); $("#colonia").val(res['colonia']); $("#cp_2").val(res['cp_2']); $("#rfc").val(res['rfc']); $("#municipio").val(res['municipio'])
    $("#uso_de_CFDI").val(res['uso_de_cfdi']); $("#metodo_de_pago").val(res['metodo_de_pago'])
}

function guardarDatos(){
    $(document).on('submit',function(event){
        event.preventDefault();
        get_inputs();
    })
}

function  get_inputs() {    

    const form = document.getElementById('formulario_queen');
    const formData = new FormData(form);           

    $.ajax({
        url:'consultas/insert_valores_del_formulario.php',
        type: 'post',
        dataType: 'json',     
        data: formData,
        processData: false,
        contentType: false,
    }).done( function (response) {  
        console.log(response[0]);

        switch (response[0]) {
            case  "listo":
                var folio = $("#folio").val()
                alert("Datos guardados con éxito");
                location.href = 'solicitud_de_ingreso_organigrama.php?name='+response[1]+'&email='+response[2]+'&folio='+folio;
                break;
            // case  "nada":
            //     location.href = 'solicitud_de_ingreso_organigrama.php?name='+response[1]+'&email='+response[2]+'&folio='+folio;
            //     break;
        }                                  
    });            
}

function saber_tipo_de_Servicio(folio) {
    $.ajax({
        url:'consultas/consulta_solicitud_de_servicios.php',
        type: 'POST',
        data: {folio: folio},
        dataType: 'json'

    }).done (function (response) {
        console.log(response);
        
        if (response[2] != "direccion_por_competencias") {
            var boton = '<button class="btn btn-success" onclick="boton_finalizar_formulario()">Finalizar</button>'
            $('.btn_siguiente').after(boton)
            $('.btn_siguiente').hide()

        }
        
    })
}

function boton_finalizar_formulario() {
    alert("Gracias por contestar el formulario, daremos el seguimiento por correo electrónico. Gracias")
    window.location.href = 'https://www.f360.com.mx/'
}

$("#btn-logout").hide()