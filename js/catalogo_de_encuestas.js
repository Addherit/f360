
function add_nueva_pregunta_click() {      
    const datos = new FormData()
    datos.append('valorEscrito', $("#id_encuesta").val()) 

    fetch('consultas/consultar_competencias.php', { method: 'POST', body: datos })
    .then(response => response.json())
    .then(response => {        
        if(response == 'false'){
            alert("Favor de agregar competencias, antes de crear preguntas")
        } else {
            var numero_de_preguntas = parseInt($("#num_preguntas").val()) + 1;                           
            if ($("#naturaleza_actual").val()) {                
                agregar_pregunta_abierta(numero_de_preguntas) 
                rellenar_slt_competencia(numero_de_preguntas, $("#id_encuesta").val())
                $("#num_preguntas").val(numero_de_preguntas)   
            } else {
                alert("completa el nombre de la naturaleza")
            }
        }
    }) 
}

function consultar_encuesta (id_encuesta) {
    const data = new FormData()
    data.append('id_encuesta', id_encuesta)
    fetch('consultas/consulta_catalogo_encuestas.php', {method: 'POST',  body: data})
    .then(response => response.json())
    .then(response => {
        if (response.length > 0) {
            asignar_numero_nuevo_consecutivo_encuestas (response)           
            rellenar_tbl_catalogo_encuestas (response)  
        } else {

            var fila_no_hay = "<tr><td colspan=6>No hay registros para mostrar</td></tr>"
            $("#tbl_catalogo_encuestas").append(fila_no_hay)
        }
    })
}

function  rellenar_tbl_catalogo_encuestas (response) {        
    $("#tbl_catalogo_encuestas tbody").empty()

    
    for(count=0; Object.keys(response).length > count; count++){                
        var contenido = '<tr>'  
            contenido += '<td><a href="#" id="btn_ver_encuesta_'+response[count].id_encuesta+'" onclick="ver_encuesta(this)"><i class="fas fa-eye"></i> Ver</a></td>'
            contenido += '<td>'+(count+1)+'</td>'
            contenido += '<td>'+response[count].id_encuesta+'</td>'
            contenido += '<td>'+response[count].titulo_encuesta+'</td>'               
            contenido += '<td>'+response[count].fecha_creacion+'</td>'
            contenido += '<td>'+response[count].estatus+'</td>'
            contenido += '</tr>'            
            
        $("#tbl_catalogo_encuestas tbody").append(contenido)       
    }
}
function ver_encuesta(ver_encuesta) {
    console.log(ver_encuesta);
    
    var ver_encuesta_id = ver_encuesta.id.substring(17)
    $("#modal_ver_encuesta").modal('show');
    
    $.ajax({
        url:'consultas/consulta_catalogo_encuestas.php',
        type: 'POST',
        data: { id_encuesta: ver_encuesta_id },
        dataType: 'JSON'
    }).done (function (response) {        
        
        $("#ver_id_encuesta").val(response[0].id_encuesta)
        $("#ver_titulo_encuesta").val(response[0].titulo_encuesta)
    })

    $.ajax({
        url:'consultas/consultar_preguntas_encuesta.php',
        type: 'POST',
        data: { id_encuesta: ver_encuesta_id },
        dataType: 'JSON'
    }).done (function (response) {
        console.log(response);        
         
        $("#ver_num_preguntas").val(response.length)
        var cuerpo_preguntas = '<table class="table table-bordered table-striped table-hover">'
        cuerpo_preguntas += '<thead><th>Pregunta</th> <th>Competencia</th> <th>Tipo</th> <th>Naturaleza</th> </thead><tbody>'
    
        for (x = 0; response.length > x; x++){

            cuerpo_preguntas += '<tr><td>'+response[x].pregunta+'</td><td>'+response[x].competencia_asignada+'</td><td>'+response[x].tipo_pregunta+'</td><td>'+response[x].nombre_naturaleza+'</td></tr>'                        
        }   

        cuerpo_preguntas +='</tbody></table>'
        $("#ver_cuerpo_encuesta_preguntas").empty()
        $("#ver_cuerpo_encuesta_preguntas").append(cuerpo_preguntas);
    })


}

function asignar_numero_nuevo_consecutivo_encuestas (response) {
     
    var id_consecutivo = parseInt(response[0].id_encuesta) + 1        
    $("#id_encuesta").val(id_consecutivo)
    $("#competencias_para_id_encuesta").val(id_consecutivo)

}

function cambio_de_tipo_pregunta (slt_id) {
    var id_select_clicado = slt_id.id
    var valor_select = document.getElementById(id_select_clicado).value;
    
    var div_a_editar = id_select_clicado.substring(3)
    div_a_editar = "div"+div_a_editar     

    switch (valor_select) {
        case 'multiple':
            $("#ranking_"+div_a_editar).empty()
            $("#multiple_"+div_a_editar).empty()
            agregar_pregunta_multiple(id_select_clicado)        
        break;

        case 'ranking':
            $("#ranking_"+div_a_editar).empty()
            $("#multiple_"+div_a_editar).empty()
            agregar_pregunta_ranking(id_select_clicado)
        break;

        case 'abierta':
            $("#ranking_"+div_a_editar).empty()
            $("#multiple_"+div_a_editar).empty()
        break;
    }
}

    function agregar_pregunta_abierta (numero_de_preguntas) {   

        
        var nueva_pregunta = '<div class="row" id="div_pregunta_gral_'+numero_de_preguntas+'">'

            nueva_pregunta += '<div class="col-5" id="div_pregunta_'+numero_de_preguntas+'">'             
            nueva_pregunta += '<input type="text" class="form-control" name="pregunta_num_'+numero_de_preguntas+'" placeholder="'+numero_de_preguntas+'. Escribe aquí la pregunta">'
            nueva_pregunta += '<input type="hidden" name="naturaleza_pregunta_'+numero_de_preguntas+'" value="'+$("#naturaleza_actual").val()+'">'
            nueva_pregunta += '</div>'

            nueva_pregunta += '<div class="col-3">'
            nueva_pregunta += '<select class="custom-select" id="slt_pregunta_competencia_'+numero_de_preguntas+'" name="slt_pregunta_competencia_'+numero_de_preguntas+'" style="width: 90%">'        
            nueva_pregunta += '</select></div>'
        
            nueva_pregunta += '<div class="col-3">'
            nueva_pregunta += '<select class="custom-select" id="slt_pregunta_'+numero_de_preguntas+'" name="slt_pregunta_'+numero_de_preguntas+'" style="width: 90%" onchange="cambio_de_tipo_pregunta(this)">'
            nueva_pregunta += '<option value="abierta" selected>Pregunta abierta</option>'
            nueva_pregunta += '<option value="multiple">Opción multiple</option>'        
            nueva_pregunta += '<option value="ranking">Ranking</option>'
            nueva_pregunta += '</select></div>'

            nueva_pregunta += '<div class="col-1" id="btn_eliminar_'+numero_de_preguntas+'" onclick="eliminar_pregunta(this)"><a href="#"><i class="fas fa-times-circle"></i></a></div></div></br>'

        $("#cuerpo_encuesta_preguntas").append(nueva_pregunta)

    }

    function agregar_pregunta_multiple (id_select_clicado) {
        var id_pregunta = id_select_clicado.substring(13)
            id_hidden_input = "pregunta_num_"+id_pregunta
            id_div_select = "div_pregunta_"+id_pregunta;    
                                                                
        var respuesta_multiple = '<div id="multiple_'+id_div_select+'"><br><p><b>Opciones multiples</b></p>'
            respuesta_multiple += '<a href="#" id="btn_nueva_opcion_'+id_div_select+'" onclick="agregar_nueva_op_multi(this)" >Agregar opcion <i class="fas fa-plus-circle"></i></a>'
            respuesta_multiple += '<input id="input_num_consecutivo_'+id_hidden_input+'" name="input_num_consecutivo_'+id_hidden_input+'" type="hidden" value="2">'
            respuesta_multiple += '<input type="text" class="form-control" name="opcion_1_pregunta_'+id_pregunta+'" placeholder="Opción 1">'
            respuesta_multiple += '<input type="text" class="form-control" name="opcion_2_pregunta_'+id_pregunta+'" placeholder="Opción 2"></div>'

        $("#"+id_div_select).append(respuesta_multiple)
    }

        function agregar_nueva_op_multi (id_btn_op_multi) {
            
            var id_pregunta = id_btn_op_multi.id.substring(30)        
            var num_consecutivo_op_multi = $("#input_num_consecutivo_pregunta_num_"+id_pregunta).val() 
            num_consecutivo_op_multi_mas_uno = parseInt(num_consecutivo_op_multi)+1
            var nueva_opcion = '<input type="text" name="opcion_'+num_consecutivo_op_multi_mas_uno+'_pregunta_'+id_pregunta+'" class="form-control" placeholder="Opción '+num_consecutivo_op_multi_mas_uno+'"></input>';
            console.log(id_div_select);
            
            $("#multiple_div_pregunta_"+id_pregunta).append(nueva_opcion)    
            $("#input_num_consecutivo_pregunta_num_"+id_pregunta).val(num_consecutivo_op_multi_mas_uno)
            
        }

    function agregar_pregunta_ranking (id_select_clicado) {

        var id_div_select = id_select_clicado.substring(12)
            id_div_select = "div_pregunta"+id_div_select;                
        var respuesta_multiple = '<div id="ranking_'+id_div_select+'"><br><select id="star_rating_'+id_div_select+'"><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option></select></div>'

        $("#"+id_div_select).append(respuesta_multiple)

        $(function() {
            $('#star_rating_'+id_div_select).barrating({
                theme: 'bars-square',           
                showValues: true,
                showSelectedRating: false
            });
        });    
    } 

function eliminar_pregunta (id_btn_eliminar) {

    var id_pregunta = id_btn_eliminar.id.substring(12)        

    $("#div_pregunta_gral"+id_pregunta).empty()
    $("#num_preguntas").val($("#num_preguntas").val()-1)  
}

function obtener_datos_nueva_encuesta () {
    
    $(document).on('submit',function(event){
        event.preventDefault();
        
        const form = document.getElementById('cuerpo_encuesta');
        const formData = new FormData(form);
        var id_encuesta_actual = $("#id_encuesta").val()

        $.ajax({
            url:'consultas/consulta_catalogo_encuestas.php',
            type:'post',
            dataType:'json',
            data: { id_encuesta: id_encuesta_actual  }
        }).done (function (response) {                         
                                        
            if($.isEmptyObject(response)) {                            
                insertar_encuesta_nueva(formData)                                
            } else {
                alert("esta en uso")
            }
                        
        })
    })        
}

function insertar_encuesta_nueva(formData) {

    $.ajax({
        url:'consultas/insertar_nueva_encuesta.php',
        type: 'post',
        // dataType: 'json',     
        data: formData,
        processData: false,
        contentType: false,
    }).done( function (response) {  
        $(".mensaje").append(response)   
        $("#modal_crear_encuesta").modal('hide')
        setTimeout('location.reload()' , 2500);
        
        
        
    })    
}

function rellenar_slt_competencia(id_pregunta, id_encuesta) {

    $.ajax({
        url:'consultas/consultar_competencias.php',
        type:'POST',
        data:{valorEscrito: id_encuesta},
        dataType:'json',
    }).done (function (response) {
        $('#slt_pregunta_competencia_'+id_pregunta).empty()
        $('#slt_pregunta_competencia_'+id_pregunta).append('<option value="" disabled selected>Selecciona una competencia</option>')
        for (x = 0; x < response.length ; x++) {
            var slt_seccion_options = '<option value="'+response[x][2]+'">'+response[x][2]+'</option>'
        $('#slt_pregunta_competencia_'+id_pregunta).append(slt_seccion_options)  
            
        }

          
    })

    
}

function escribir_naturaleza_actual(escritura) {    
    $("#naturaleza_actual").val(escritura.value)
}

function agregar_naturaleza() {

    var naturaleza_actua = $("#naturaleza_actual").val() 
    $("#naturaleza_anterior").val("")
    $("#naturaleza_anterior").val(naturaleza_actua)
     
    var naturaleza_siguiente = parseInt($("#num_naturalezas").val()) + 1    
    var naturaleza = '<div class="row" id="naturaleza_num_'+naturaleza_siguiente+'">'
    naturaleza += '<div class="col-11"><input type="text" class="titulo-naturaleza" placeholder="Nombre de la Naturaleza" onkeyup="escribir_naturaleza_actual(this)"></div>'
    naturaleza += '<div class="col-1" id="'+naturaleza_siguiente+'" onclick="eliminar_naturaleza(this)"><a href="#"><i class="fas fa-times-circle"></i></a></div>'
    naturaleza += '</div>'

    $("#cuerpo_encuesta_preguntas").append(naturaleza);
    $("#naturaleza_actual").val("")
    $("#num_naturalezas").val(naturaleza_siguiente)
}

function agregar_competencias_desde_modal() {
    var num_consecutivo_competencia = parseInt($("#num_competencia_modal").val()) + 1
    var competencias_nuevas_desde_modal = '<div class="form-row">'

    competencias_nuevas_desde_modal += '<div class="form-group col-md-8">'
    competencias_nuevas_desde_modal += '<input name="nombre_competencia_'+num_consecutivo_competencia+'" type="text" class="form-control" placeholder="Ingresa el nombre para la competencia" required>'
    competencias_nuevas_desde_modal += '<label for="descripcion_competencia">Descripción de la competenia</label>'
    competencias_nuevas_desde_modal += '<textarea class="form-control" name="descripcion_competencia_'+num_consecutivo_competencia+'" id="" cols="75" rows="2"></textarea>'
    competencias_nuevas_desde_modal += '</div></div> <hr>'
    $("#cuerpo_competencias_modal").append(competencias_nuevas_desde_modal)
    $("#num_competencia_modal").val(num_consecutivo_competencia)
}

function eliminar_naturaleza(id_naturaleza) {   

    if (id_naturaleza.id == $("#num_naturalezas").val()) {
        $("#naturaleza_num_"+id_naturaleza.id).empty()      
        $("#naturaleza_actual").val("")
        $("#naturaleza_actual").val($("#naturaleza_anterior").val())

    } else {
        alert("esta ya no se puede elimianr")
    }

        
}
