<!-- modal mandar formulario -->
<div class="modal fade" id="modal_mandar_formulario" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Mandar correo electrónico</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Escribe el <b>correo eletrónico de la persona</b> a la cual se le enviará la solicitud de servicios</p>
        <br>     
        <div class="mensaje-modal-enviar-correo"></div>   
        <div class="input-group">
          <div class="input-group-prepend">
            <div class="input-group-text"><i class="far fa-envelope"></i></div>
          </div>
          <input type="text" class="form-control" id="email_escrito" placeholder="correo electrónico">
        </div>
        <br>
        <br>
        <div class="row">
          <div class="col-md-12">
            <h5>Selecciona el tipo de servicio</h5>
          </div>                  
          <div class="col-md-12">

            <div class="form-check">
              <input class="form-check-input" type="radio" name="tipo_servicio" id="direccion_por_competencias" value="direccion_por_competencias">
              <label class="form-check-label" for="direccion_por_competencias">
                Dirección por competencias
              </label>
            </div>

            <div class="form-check">
              <input class="form-check-input" type="radio" name="tipo_servicio" id="encuestas_360" value="encuestas_360">
              <label class="form-check-label" for="encuestas_360">
                Encuestas 360°
              </label>
            </div>

            <div class="form-check">
              <input class="form-check-input" type="radio" name="tipo_servicio" id="360_consulting" value="360_consulting">
              <label class="form-check-label" for="360_consulting">
                360° consulting
              </label>
            </div>           
            
          </div>       
        </div>
        <br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-success" id="btn_enviar_correo" onclick="enviar_correo_segun_servicio_solicitado()">Enviar</button>
      </div>
    </div>
  </div>
</div>

<!-- modal crear encuesta -->
<div class="modal fade" id="modal_crear_encuesta" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Crear encuesta</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">  
        <form id="cuerpo_encuesta" method="post" enctype="multipart/form-data" onsubmit="obtener_datos_nueva_encuesta()" class="formulario">  

          <div class="row">            
            <label for="staticEmail" class="col-4 col-md-3 offset-md-7 col-form-label"><b>ID encuesta:</b></label>
            <div class="col-2">
              <input type="text" name="id_encuesta" readonly class="form-control-plaintext" id="id_encuesta">
            </div>
          </div>

          <div class="row">
            <label for="staticEmail" class="col-5 col-md-3 offset-md-7 col-form-label"><b>Numero de preguntas:</b></label>
            <div class="col-2">
              <input type="text" readonly class="form-control-plaintext" id="num_preguntas" name="num_preguntas" value="0">   
              <input type="hidden" id="num_naturalezas" name="num_naturalezas" value="1">
            </div>
          </div> 
          <section style="background-color: #efefef; padding: 5px; border-radius: 10px; max-height: 490px; overflow: auto;">
            <div class="form-group row">
              <div class="col-12">
                <input type="text" id="titulo_encuesta" name="titulo_encuesta"  class="text-center" placeholder="Titulo de la encuesta"> 
              </div>
              
            </div>            
            <div class="col-12">
              <p style="color: grey">
                <b>bienvenido a la encuesta 360 es de caracter totalmete personal, te pediomos nos apoyes contestandola lo mas objetivamente posible siendo la escala de 1 menor valor y 5 mayor valor </b>
              </p>
            </div>  
            <div id="cuerpo_encuesta_preguntas">
              <input type="hidden" id="naturaleza_actual">
              <input type="hidden" id="naturaleza_anterior">
              <div class="col-md-12">
                <input type="text" class="titulo-naturaleza" placeholder="Nombre de la Naturaleza" onkeyup="escribir_naturaleza_actual(this)">
              </div>
            </div>                                      
            <br>
            <br>
            <div class="row">
              <div class="col-md-6">
                <input class="btn btn-block btn-lg btn-secondary" id="btn_agregar_nueva_pregunta" type="button" value="Agregar nueva pregunta" onclick="add_nueva_pregunta_click()">
              </div>           
              <div class="col-md-6">
                <input class="btn btn-block btn-lg btn-secondary" id="btn_agregar_nueva_naturaleza" type="button" value="Agregar nueva naturaleza" onclick="agregar_naturaleza()">
              </div>
            </div>  
          </section>         
            <br>            
      </div>

      <div class="modal-footer">
        <div class="col-6 text-left">
          <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_crear_competencias">Agregar competencias</button>
        </div>
        <div class="col-6 text-right">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
          <button type="submit" class="btn btn-success" id="btn_crear_encuesta">Crear</button>
        </div>
      </form>   
      </div>
    </div>
  </div>
</div>

<!-- modal ver encuesta -->
<div class="modal fade" id="modal_ver_encuesta" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ver encuesta</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">  
        <form id="cuerpo_encuesta" method="post" enctype="multipart/form-data" onsubmit="obtener_datos_nueva_encuesta()" class="formulario">  

          <div class="row">
            <label for="staticEmail" class="col-6 col-sm-3 offset-4 offset-sm-7 col-form-label"><b>ID encuesta:</b></label>
            <div class="col-2">
              <input type="text" class="form-control-plaintext" id="ver_id_encuesta" readonly>
            </div>
          </div> 

          <div class="form-group row">
            <label for="staticEmail" class="col-8 col-sm-3 offset-2 offset-sm-7 col-form-label"><b>Numero de preguntas:</b></label>
            <div class="col-2">
              <input type="text" class="form-control-plaintext" id="ver_num_preguntas" readonly>              
            </div>
          </div>  
            <div class="form-group row">
              <input type="text" id="ver_titulo_encuesta" class="text-center" placeholder="No hay titulo" readonly> 
            </div>                        
            <div class="col-12">
              <p style="color: grey">
                <b>bienvenido a la encuesta 360 es de caracter totalmete personal, te pediomos nos apoyes contestandola lo mas objetivamente posible siendo la escala de 1 menor valor y 5 mayor valor </b>
              </p>
            </div>  
            <div id="ver_cuerpo_encuesta_preguntas"></div>            
            <br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>        
      </form>   
      </div>
    </div>
  </div>
</div>

<!-- modal crear competencia -->
<div class="modal fade" id="modal_crear_competencias" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Crear competencia</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <form id="form_crear_competencia" onsubmit="crear_nueva_competencia()">
        <input type="hidden" name="num_competencia_modal" id="num_competencia_modal" value="1">
        <input type="hidden" name="competencias_para_id_encuesta" id="competencias_para_id_encuesta">

        <div class="modal-body">
        <div class="mensaje_modal_competencias"></div>          
          <div id="cuerpo_competencias_modal" style="background-color: #efefef; padding: 5px; border-radius: 10px; max-height: 490px; overflow: auto;">
            <div class="form-row">
              <div class="form-group col-md-8">
                <label for="nombre_competencia">Nombre de la competenia</label>
                <input name="nombre_competencia_1" type="text" class="form-control" placeholder="Ingresa el nombre para la competencia">
              </div>            
            </div>
            <div class="form-row">
              <div class="form-group col-md-8">
                <label for="descripcion_competencia">Descripción de la competenia</label>
                <textarea class="form-control" name="descripcion_competencia_1" id="" cols="75" rows="2"></textarea>                
              </div>            
            </div>
            <hr>
          </div>                          
          <div class="row">
            <div class="col-md-12">
              <input class="btn btn-block btn-lg btn-secondary" type="button" value="Agregar nueva competencia" onclick="agregar_competencias_desde_modal()">
            </div>
          </div>  
        </div>      
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
          <button type="submit" class="btn btn-success" >Guardar competencias</button>      
        </div>
      </form>
    </div>
  </div>
</div>

<!-- modal editar competencia -->
<div class="modal fade" id="modal_editar_competencias" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Editar competencia # <span class="id_competencia_a_editar"></span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <form id="form_editar_competencia" onsubmit="editar_competencia()">

        <div class="modal-body">      
          <input type="hidden" id="id_competencia_a_editar_modal_editar" name="id_competencia_a_editar_modal_editar">        
          <div class="mensaje_modal_editar_competencia"></div>

          <div class="form-group row">
            <label for="estatus_competencia_modal_editar" class="col-sm-2 offset-sm-5 col-form-label text-right">Estatus</label>
            <div class="col-sm-4">
              <select name="estatus_competencia_modal_editar" id="estatus_competencia_modal_editar" class="form-control">
                <option value="ACTIVO">ACTIVO</option>
                <option value="INACTIVO">INACTIVO</option>
              </select>
            </div>
          </div>

          <div class="form-group row">
            <label for="nombre_competencia_modal_editar" class="col-sm-3 offset-sm-1 col-form-label">Nombre de competencia</label>
            <div class="col-sm-7">
              <input type="text" class="form-control" id="nombre_competencia_modal_editar" name="nombre_competencia_modal_editar">
            </div>
          </div>

          <div class="form-group row">
            <label for="naturaleza_competencia" class="col-sm-3 offset-sm-1 col-form-label">ID de encuesta</label>
            <div class="col-sm-7">
              <input type="text" id="editar_naturaleza_competencia" class="form-control" readonly>                         
            </div>
          </div>          

          <div class="form-group row">
            <label for="descripcion_textarea_competencia" class="col-sm-3 offset-sm-1 col-form-label">Descripción</label>
            <div class="col-sm-7">
              <textarea name="descripcion_textarea_competencia" class="form-control" cols="30" rows="4" style="background-color: #ffffd7"></textarea>                                     
            </div>
          </div>                    
        
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
          <button type="submit" class="btn btn-success" id="btn_editar_competencia">Editar</button>      
        </div>
      </form>
    </div>
  </div>
</div>

<!-- modal crear seccion -->
<div class="modal fade" id="modal_crear_seccion" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Crear Sección</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <form id="form_crear_seccion" onsubmit="crear_nueva_seccion()">

        <div class="modal-body">  
          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="tipo_seccion">Tipo de sección</label>
              <select name="tipo_seccion" id="tipo_seccion" class="form-control">
                <option value="competencias_directivas">Competencias Directivas</option>
              </select>
            </div>
            <div class="form-group col-md-6">
              <label for="nombre_seccion">Nombre de la sección</label>
              <input name="nombre_seccion" type="text" class="form-control" id="nombre_seccion" placeholder="Ingresa el nombre para la seccion">
            </div>
          </div>
        </div>      
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
          <button type="submit" class="btn btn-success" id="btn_crear_seccion">Crear Sección</button>      
        </div>
      </form>
    </div>
  </div>
</div>

<!-- modal notificaciones de servicio -->
<div class="modal fade" id="modal_notificaciones_de_servicio" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Notificaciones de servicio # <span id="id_notificaciones_de_Servicio_editar"></span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

        <div class="modal-body"> 
          <div class="mensaje_modal"></div>
          <form id="form_editar_solicitud_de_servicio" onsubmit="editar_solicitud_de_servicios()">
        
            <input type="hidden" id="id_solicitud_servicio_modal_hide" name="id_solicitud_servicio_modal_hide">  
            <div class="row">        
              <div class="col-md-6">
                <div class="row">
                  <div class="col-12 text-center">
                    <h5>Datos del solicitante</h5>
                  </div>
                </div>
                <div class="row">
                  <div class="col-12">   
                    <div class="input-group form-group">
                      <div class="input-group-prepend">
                          <span class="input-group-text"><i class="fas fa-user"></i></span>
                      </div>
                      <input type="text" id="persona_solicitante_del_servicio" name="persona_solicitante_del_servicio"  class="form-control form-control-sm" readonly>                                        
                    </div>   
                  </div> 
                </div>
                <div class="row">
                  <div class="col-12">
                    <div class="input-group form-group">
                      <div class="input-group-prepend">
                          <span class="input-group-text"><i class="fas fa-at"></i></span>
                      </div>
                      <input type="text" id="correo_persona_solicitante_modal_editar" name="correo_persona_solicitante_modal_editar"  class="form-control form-control-sm" readonly>                                        
                    </div>      
                  </div>
                </div>
              </div>      
              <div class="col-md-6 mb-3">
                <div class="col-12 text-center">
                  <h5>Fecha de solicitud</h5>
                </div>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-calendar-alt"></i></span>
                  </div>
                  <input type="text" class="form-control form-control-sm" id="fecha_solicitud_modal_editar" name="fecha_solicitud_modal_editar" readonly>               
                </div>
              </div>
            </div>
            <br>

            <div class="form-group row text-center">   
              <div class="col-md-12">
                <h5>Encuesta asignada</h5>
                <select id="slt_encuestas_disponibles_modal_editar" class="form-control form-control-sm" name="slt_encuestas_disponibles_modal_editar">
                  <option value="" selected disabled>Seleccionar una encuesta</option>
                </select>
              </div>
            </div>

            <div class="row">
              <div class="col-md-8">
                <table id="tbl_equipo_solicitante_modal_editar" class="table table-bordered table-striped table-hover table-sm">
                  <thead>
                    <tr>
                      <th></th>
                      <th>Nombre participante</th>
                      <th>Correo electrónico</th>
                      <th>Ya contestó</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td colspan="4" class="text-center">No hay resultados para mostrar</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="col-md-4">
                <div class="form-row">
                  <div class="col-md-12 mb-3">
                    <label for="estatus_notificacion_servicio_editar"><b>Estatus</b></label>
                    <input type="text" class="form-control form-control-sm" id="estatus_notificacion_servicio_editar" name="estatus_notificacion_servicio_editar" disabled>
                  </div>            
                </div>
                <hr>
                <div class="row">
                  <div class="col-12">                
                    <canvas id="encuestas_contestadas" width="450" style="border-radius: 5px; background-color: #f0f0f0"></canvas>
                  </div>
                </div>
              </div>
              <div class="col-12">
                <a id="btn-vista-previa"><button type="button" class="btn btn-primary">Vista previa, respuestas</button></a>
              </div>
            </div>       
            <br>
        </div>      
        <div class="modal-footer">        
          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
          <button type="submit" class="btn btn-success" id="btn_editar_solicitud_servicio">Editar competencia</button>      
        </div>
      </form>
    </div>
  </div>
</div>

<!-- modal vista previa de respuestas -->
<div class="modal fade" id="modal_vista_previa_servicios" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Crear comentarios finales</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      

        <div class="modal-body">  
          <h1>Plan de mejora</h1>
          <h3>El plan que propone para trabajar las áreas de oportunidad es el siguiente:</h3>
          <hr>
          <table id="tbl-fortalezas" class="table table-sm table-hover table-striped table-bordered">
            <thead class="text-center">
              <tr>
                <th colspan="2">Fortalezas</th>
              </tr>
              <tr>
                <th>Competencia</th>
                <th>Observación</th>
              </tr>
            </thead>
            <tbody></tbody>
          </table>
          <hr>
          <table id="tbl-comentarios-finales" class="table table-bordered table-striped table-hover table-sm">
            <thead class="text-center">
              <tr><th colspan=2>Comentarios finales</th></tr>
              <tr>
                <th></th>
                <th>Planteamiento</th>
              </tr>
            </thead>

            <tbody>
              <tr>
                <td>Coaching Directivo</td>
                <td contenteditable></td>
              </tr>
              <tr>
                <td>Acción Personal</td>
                <td contenteditable></td>
              </tr>
              <tr>
                <td>Desarrollo Personal</td>
                <td contenteditable></td>
              </tr>
            </tbody>
          </table>
        </div>      
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
          <button type="submit" class="btn btn-success" onclick="btn_guardar_comentarios()">Guardar comentarios</button>      
        </div>
    </div>
  </div>
</div>