<?php 

    if(isset($_GET['id_encuesta'])){
        $id_encuesta = $_GET['id_encuesta'];
        $persona_asingnada = $_GET['persona_asignada'];
        $id_servicio = $_GET['id_servicio'];
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>F360</title>
</head>
<body onload="consultar_encuesta()">
    <?php include "header.php" ?>
    <div class="container">                 
        <?php include "modales.php"?>          
        <div class="row">
            <div class="col-sm-12">
            <!-- <input type="text" id="titulo_encuesta" value="Sin nombre de encuesta"> -->
                <h1 id="titulo_encuesta_que_se_borrara"></h1>
            </div>            
        </div>
        <br>
        <div class="col-sm-12">
            <form id="form_encuesta_respuestas" onsubmit="finalizar_encuesta()">
                <div id="contenido_preguntas">
                <input type="hidden" id="id_servicio_solicitado" name="id_servicio_solicitado" value="<?php echo $id_servicio ?>">    
                    <input type="hidden" id="id_encuesta_para_contestar" name="id_encuesta_para_contestar" value="<?php echo $id_encuesta ?>">                        
                    <input type="hidden" id="persona_asignada_en_link" name="persona_asignada_en_link" value="<?php echo $persona_asingnada ?>">
                    <input type="hidden" id="total_respuestas" name="total_respuestas">
                    <input type="hidden" id="es_lider_input_hidden" name="es_lider_input_hidden">
                </div>
                <div>                
                    <button type="submit" class="btn btn-success">Finalizar encuesta</button>
                </div>
                <br>
            </form>        
        </div>                                     
    </div>
    <!-- no se pone con footer.php porque cargaba demaciado lento el charts.js -->
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/fontawesome-all.js"></script>
    <script src="js/contestar_encuesta.js"></script>
    <script src="jquery-bar-rating/jquery.barrating.js"></script>
</body>
</html>