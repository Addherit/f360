<!DOCTYPE html>
<html lang="en">
<?php include "header.php" ?>
<body onload="consultar_encuesta (0)">
    <?php include "nav.php" ?>  
    <div class="d-flex" id="wrapper">
        <?php include "sidebar.php"?>   
        <div id="page-content-wrapper">   
            <?php include "modales.php"?>    
            <div class="container-fluid">  
                <div class="row">
                    <div class="col-12 d-flex flex-wrap flex-md-nowrap align-items-center pt-3 mb-3 border-bottom">

                        <div class="col-sm-8">
                            <button class="btn" id="btn-sidebar" title="Campos disponibles"><i class="fas fa-bars"></i></button>
                            <h1 class="h2">Catálogo de encuestas</h1>
                        </div>
                        <div class="col-sm-4 text-right">
                            <button class="btn btn-sm btn-info" data-toggle="modal" data-target="#modal_crear_encuesta">Crear encuesta</button>
                        </div>
                        
                    </div>  
                </div>                      
                <div class="col-md-12">
                    <div class="mensaje"></div> 
                    <div class=" table-responsive"> 
                        <table id="tbl_catalogo_encuestas" class="table table-sm table-striped table-hover text-center table-bordered" style="white-space: nowrap">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>N°</th>
                                    <th>Id</th>
                                    <th>Tipo de encuesta</th>
                                    <th>Fecha creación</th>
                                    <th>Estatus</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>                                              
            </div>
        </div>
    </div>
    <?php include "footer.php" ?>
    <script src="js/catalogo_de_encuestas.js"></script>
    <script src="js/competencias.js"></script>
</body>
</html>